#!/usr/bin/python3

import logging

import argparse
import re
import sys

from fabric_tpa.gitlab import GitLabConnector
from gitlab.exceptions import GitlabError, GitlabListError

# all supported webhook events, taken from
# https://docs.gitlab.com/ee/user/project/integrations/webhook_events.html
#
# actual names here come from the API response itself and can be also
# found in the web UI HTML form
WEBHOOK_EVENTS = [
    "confidential_issues_events",
    "confidential_note_events",
    "deployment_events",
    "issues_events",
    "job_events",
    "merge_requests_events",
    "note_events",
    "pipeline_events",
    "push_events",
    "releases_events",
    "tag_push_events",
    "wiki_page_events",
]

# the events that should trigger by default when creating a hook
#
# by default, it seems like either *all* events or *no* events trigger
# a hook, unclear.
WEBHOOK_EVENTS_DEFAULTS = [
    "issues_events",
    "merge_requests_events",
    "pipeline_events",
    "push_events",
    "releases_events",
    "tag_push_events",
    "wiki_page_events",
]


class LoggingAction(argparse.Action):
    """change log level on the fly

    The logging system should be initialized before this, using
    `basicConfig`.

    Example usage:

    parser.add_argument(
        "-v",
        "--verbose",
        action=LoggingAction,
        const="INFO",
        help="enable verbose messages",
    )
    parser.add_argument(
        "-d",
        "--debug",
        action=LoggingAction,
        const="DEBUG",
        help="enable debugging messages",
    )
    """

    def __init__(self, *args, **kwargs):
        """setup the action parameters

        This enforces a selection of logging levels. It also checks if
        const is provided, in which case we assume it's an argument
        like `--verbose` or `--debug` without an argument.
        """
        kwargs["choices"] = logging._nameToLevel.keys()
        if "const" in kwargs:
            kwargs["nargs"] = 0
        super().__init__(*args, **kwargs)

    def __call__(self, parser, ns, values, option_string=None):
        """if const was specified it means argument-less parameters"""
        if self.const:
            logging.getLogger("").setLevel(self.const)
        else:
            logging.getLogger("").setLevel(values)


class GitlabHooksArgumentParser(argparse.ArgumentParser):
    def __init__(self, *args, **kwargs):
        super().__init__(*args, **kwargs)
        self.add_argument(
            "-q",
            "--quiet",
            action=LoggingAction,
            const="WARNING",
            help="Silence informational messages",
        )
        self.add_argument(
            "-d",
            "--debug",
            action=LoggingAction,
            const="DEBUG",
            help="Enable debugging messages",
        )
        self.add_argument(
            "-n",
            "--dry-run",
            action="store_true",
            help="Only show what would be done, do not create, change or delete anything",
        )
        self.add_argument(
            "-i",
            "--instance",
            action="store",
            help="GitLab instance identifier from configuration file, default specified in configuration file",
        )
        self.add_argument(
            "-c",
            "--config-file",
            nargs="+",
            help="Path to the python-gitlab configuration file, default specified in https://python-gitlab.readthedocs.io/en/stable/cli-usage.html#configuration-files",  # noqa: E501
        )
        self.add_argument(
            "-f",
            "--url-filter",
            action="store",
            help="only consider webhooks whose URL match this regexp filter",
        )
        group = self.add_mutually_exclusive_group(required=True)
        group.add_argument(
            "-a",
            "--all-projects",
            action="store_true",
            help="work on all projects",
        )
        group.add_argument(
            "-p",
            "--project",
            action="append",
            dest="projects",
            help="which project to work on",
        )
        group.add_argument(
            "-g",
            "--group",
            action="append",
            dest="groups",
            help="which groups to work on",
        )

        subparsers = self.add_subparsers(
            dest="task", parser_class=argparse.ArgumentParser, required=True
        )

        subparsers.add_parser("list", help="list existing webhooks")
        subparsers.add_parser("audit", help="audit existing webhooks")

        create_task = subparsers.add_parser("create", help="create a new webhook")
        create_task.add_argument(
            "--url",
            action="store",
            required=True,
            help="hook url",
        )
        create_task.add_argument(
            "--tls-verification",
            dest="enable_ssl_verification",
            action=argparse.BooleanOptionalAction,
            default=True,
            help="if the triggered hook should do TLS verification, default: %(default)s",
        )
        create_task.add_argument(
            "--token",
            action="store",
            help="secret token to validate received payloads",
        )
        for event in WEBHOOK_EVENTS:
            create_task.add_argument(
                f"--{event}".replace("_", "-"),
                action=argparse.BooleanOptionalAction,
                default=event in WEBHOOK_EVENTS_DEFAULTS,
                help="trigger hook for %s" % event.replace("_", " "),
            )
        create_task.add_argument(
            "--push-events-branch-filter",
            dest="push_events_branch_filter",
            action="store",
            help="trigger hook on push events for matching branches only",
        )

        subparsers.add_parser("delete", help="delete all matching webhooks")

        update_task = subparsers.add_parser(
            "update", help="update configuration of existing webhooks"
        )
        update_task.add_argument(
            "--url",
            action="store",
            default=argparse.SUPPRESS,
            help="hook url",
        )
        update_task.add_argument(
            "--tls-verification",
            dest="enable_ssl_verification",
            action=argparse.BooleanOptionalAction,
            default=argparse.SUPPRESS,
            help="if the triggered hook should do TLS verification, default: unchanged",
        )
        update_task.add_argument(
            "--token",
            action="store",
            default=argparse.SUPPRESS,
            help="secret token to validate received payloads",
        )
        for event in WEBHOOK_EVENTS:
            update_task.add_argument(
                f"--{event}".replace("_", "-"),
                dest=event,
                action=argparse.BooleanOptionalAction,
                default=argparse.SUPPRESS,
                help="trigger hook for %s" % event.replace("_", " "),
            )
        update_task.add_argument(
            "--push-events-branch-filter",
            dest="push_events_branch_filter",
            action="store",
            default=argparse.SUPPRESS,
            help="trigger hook on push events for matching branches only",
        )

    def parse_args(self, *args, **kwargs):
        args = super().parse_args(*args, **kwargs)
        logging.debug("parsed arguments: %s", args)
        return args

    def projects_iterator(self, args):
        try:
            gitlab_connector = GitLabConnector(
                args.instance,
                args.config_file,
            )
        except GitlabError as e:
            config_file_path = args.config_file or "~/.python-gitlab.cfg"
            # if this happens, recreate the group access token in:
            #
            # https://gitlab.torproject.org/groups/tpo/tpa/-/settings/access_tokens
            #
            # with the following properties:
            # - name: gitlab-tools-webhook-audit
            # - delete expiration time (means one year)
            # - role: maintainer
            # - scope: api
            #
            # then copy-paste the secret into:
            # https://gitlab.torproject.org/tpo/tpa/gitlab-tools/-/settings/ci_cd
            self.error(
                "failed to login to GitLab: %s - maybe the token in %s expired?"
                % (e, config_file_path)
            )

        if args.all_projects:
            projects = gitlab_connector.projects()
        elif args.projects:
            projects = gitlab_connector.projects(args.projects)
        elif args.groups:
            projects = gitlab_connector.group_projects(args.groups, archived=False)
        else:
            self.error("no project, group or all projects specified")

        logging.debug("loaded projects %s", projects)
        return projects


def hooks_compute_parameters(args):
    """compute the parameters to pass to hook creation or update

    I am honestly not quite sure what this does. I *think* it takes
    whatever is after the "task" (update / create / delete) and makes
    a dict out of the key/value pairs of the parsed argparse
    dictionnary.

    But I'm not certain.
    """

    parameters = {}
    position = 1
    for key, _ in vars(args).items():
        if key == "task":
            parameters = dict(list(vars(args).items())[position:])
            break
        position += 1
    return parameters


def list_hooks(project_name, hook):
    extras = [
        k.replace("_events", "")
        for k, v in hook.attributes.items()
        if k.endswith("_events") and v
    ]

    if hook.attributes.get("disabled_until") is not None:
        logging.warning(
            "project %s hook %s disabled until %s"
            % (project_name, hook.id, hook.attributes.get("disabled_until"))
        )
        extras.append("DISABLED")
    print("%s\t\t%s\t%s" % (project_name, hook._attrs.get("url"), extras))


def main():
    logging.basicConfig(format="%(levelname)s: %(message)s", level="INFO")
    parser = GitlabHooksArgumentParser()
    args = parser.parse_args()

    if args.url_filter:
        filter_regexp = re.compile(args.url_filter)
    else:
        filter_regexp = None

    parameters = hooks_compute_parameters(args)
    projects_without_hooks = []

    for project in parser.projects_iterator(args):
        project_name = project.path_with_namespace
        logging.info("inspecting project %s", project_name)

        if args.task == "create":
            if args.dry_run:
                logging.warning(
                    "would create webhook in project %s with parameters %s",
                    project_name,
                    parameters,
                )
            else:
                logging.warning(
                    "creating webhook in project %s with parameters %s",
                    project_name,
                    parameters,
                )
                project.hooks.create(parameters)

            continue

        try:
            hooks_list = project.hooks.list()
        except GitlabListError as e:
            logging.warning(
                "error loading hooks list on project %s: %s - likely not enough permissions",
                project_name,
                e,
            )
            continue

        if not hooks_list:
            logging.warning("no hook found in project %s", project_name)
            projects_without_hooks.append(project_name)
            continue

        for hook in hooks_list:
            if filter_regexp and not re.search(filter_regexp, hook.url):
                continue

            logging.debug("found hook %s", hook)
            if args.task == "list":
                list_hooks(project_name, hook)
                continue

            elif args.task == "delete":
                if args.dry_run:
                    logging.warning(
                        "would delete project %s webhook [%d]", project_name, hook.id
                    )
                else:
                    logging.warning(
                        "deleting project %s webhook [%d]", project_name, hook.id
                    )
                    hook.delete()
                continue

            elif args.task == "update":
                if args.dry_run:
                    logging.warning(
                        "would update project %s webhook [%d] with parameters %s",
                        project_name,
                        hook.id,
                        parameters,
                    )
                else:
                    logging.warning(
                        "updating project %s webhook [%d] with parameters %s",
                        project_name,
                        hook.id,
                        parameters,
                    )
                    for key, value in parameters.items():
                        setattr(hook, key, value)
                    hook.save()

                continue

    if args.task == "audit":
        if projects_without_hooks:
            logging.error(
                "the following projects do *not* have hooks configured: %s",
                " ".join(projects_without_hooks),
            )
            sys.exit(1)


if __name__ == "__main__":
    main()
