#!/usr/bin/python3

from importlib import import_module
import logging
import os

# expose all modules to fabric
from invoke import Collection

# get the safe_open hack
from fabric_tpa import ui  # noqa: F401

collection = []
for mod in [
    "fabric_tpa.fleet",
    "fabric_tpa.ganeti",
    "fabric_tpa.git",
    "fabric_tpa.gitlab",
    "fabric_tpa.gitolite",
    "fabric_tpa.host",
    "fabric_tpa.install",
    "fabric_tpa.kgb",
    "fabric_tpa.libvirt",
    "fabric_tpa.mailman",
    "fabric_tpa.mandos",
    "fabric_tpa.prometheus",
    "fabric_tpa.reboot",
    "fabric_tpa.retire",
    "fabric_tpa.silence",
    "fabric_tpa.user",
]:
    try:
        collection.append(import_module(mod))
    except Exception as e:
        print("WARNING: skipping task collection for", mod, ":", str(e))

# setup some sane logging
#
# by default, Fabric doesn't enable "verbose" (logging.INFO) logging,
# which makes it hard to have some interactive output witouth yelling
# WARNING all the time.
#
# i've had a PR pending since forever to improve this, to allow the
# loglevel to be changed in the fly from the `fab` (or `invoke`)
# executable:
#
# https://github.com/pyinvoke/invoke/pull/706
#
# this is what the fabric_tpa.VerboseProgram does as well, but doesn't
# work when we call tasks directly with the `fab` binary.
#
# this is a bit different: it changes the default to INFO. it can be
# tweaked to something else with the FABRIC_LOGLEVEL environment
# (something completely specific to this) to add some external knob,
# but ideally this would just be merged upstream.
#
# we also change the log format a bit here. it's unclear what the
# format actually is in invoke: invoke.util.LOG_FORMAT is
# "%(name)s.%(module)s.%(funcName)s: %(message)s" but what I actually
# see in the output also includes the "%(levelname)s" so I'm a bit
# confused. I find the module.funcName stuff to be way too noisy, so I
# redact it out.
logging.basicConfig(
    level=os.environ.get("FABRIC_LOGLEVEL", "INFO"), format="%(levelname)s: %(message)s"
)
# override default logging policies in submodules
#
# without this, we get debugging info from paramiko with --verbose
for mod in "fabric", "paramiko", "invoke":
    logging.getLogger(mod).setLevel("WARNING")

ns = Collection(*collection)
