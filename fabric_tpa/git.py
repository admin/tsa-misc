import shlex
import logging
import subprocess
from typing import Optional

from fabric import task, Connection

from fabric_tpa.ui import yes_no


def list_all_repos(
    con: Connection,
    repos_path: str = "/srv/git.torproject.org/repositories/",
):
    """help function to list repositories in the given directory

    This is separate from the task to be easier to call."""
    logging.info(
        "listing repositories in %s on %s",
        repos_path,
        getattr(con, "host", "localhost"),
    )
    ret = con.run("find %s -type d -a -name '*.git'" % repos_path, hide=True, warn=True)
    if not ret.ok:
        if ret.stdout:
            logging.warn(
                "find failed with status %d, continuing anyway: ",
                ret.exited,
                ret.stderr.strip(),
            )
        else:
            logging.warn(
                "find failed with status %d: %s", ret.exited, ret.stderr.strip()
            )
            return

    for line in ret.stdout.splitlines():
        assert repos_path in line
        yield line.strip()


def list_public_repos(
    con: Connection,
    repos_path: str = "/srv/git.torproject.org/repositories/",
):
    """help function to list repositories on the remote gitolite server

    This is separate from the task to be easier to call."""
    logging.info(
        "listing repositories with the git-daemon-export-ok flag in %s",
        repos_path,
        getattr(con, "host", "localhost"),
    )
    ret = con.run(
        "find %s -type f -a -name 'git-daemon-export-ok'" % repos_path,
        hide=True,
        warn=True,
    )
    if not ret.ok:
        if ret.stdout:
            logging.warn(
                "find failed with status %d, continuing anyway: ",
                ret.exited,
                ret.stderr.strip(),
            )
        else:
            logging.warn(
                "find failed with status %d: %s", ret.exited, ret.stderr.strip()
            )
            return

    for line in ret.stdout.splitlines():
        assert repos_path in line
        yield line.strip().removesuffix("/git-daemon-export-ok")


@task
def list_repos(
    con: Connection,
    repos_path: str = "/srv/git.torproject.org/repositories/",
    public_only: bool = False,
):
    """list repositories on the given git server"""
    # TODO: this would be prettier if invoke autoprinted generators
    # correctly, and this stub would just not be necessary at all:
    # https://github.com/pyinvoke/invoke/issues/992
    if public_only:
        for repo in list_public_repos(con, repos_path):
            print(repo)
    else:
        for repo in list_all_repos(con, repos_path):
            print(repo)


def commit_and_push(repository: str, path: str, commit_msg):
    try:
        subprocess.check_call(["git", "-C", repository, "diff", path])
        git_commit_cmd = ["git", "-C", repository, "commit", "-F", "-", path]
        git_push_cmd = ["git", "-C", repository, "push"]
        if yes_no("commit and push above changes in %s?" % repository):
            logging.info("committing %s", path)
            subprocess.run(
                git_commit_cmd, input=commit_msg, check=True, encoding="utf-8"
            )
            logging.info("pushing in %s", repository)
            subprocess.check_call(git_push_cmd)
        else:
            logging.warning("aborting commit and push")
            logging.info(
                'use "%s && %s" to commit and push',
                shlex.join(git_commit_cmd),
                shlex.join(git_push_cmd),
            )
    except subprocess.CalledProcessError as e:
        logging.error("failed to push or commit: %s", e)


@task
def is_empty_repo(con: Connection, repo_path: Optional[str] = None, sudo: bool = False):
    """if the given repo (or current directory) is an empty git repository

    An empty git repository is defined as a repository without any
    commits, including those in a pack file.
    """
    git_cmd = ""
    if sudo:
        git_cmd = "sudo -u git "

    if repo_path is None:
        git_cmd += "git"
    else:
        git_cmd += f"git -C {repo_path}"
    ret = con.run(f"{git_cmd} count-objects -v", hide=True, warn=True)
    if not ret.ok:
        logging.warning("cannot run `git count-objects -v` on repository %s", repo_path)
    non_empty_stats = []
    for stat in ret.stdout.splitlines():
        key, value = stat.split(": ")
        if int(value) > 0:
            non_empty_stats.append(stat)
    if non_empty_stats:
        logging.info("repository is not empty: %s", ", ".join(non_empty_stats))
    else:
        logging.info("repository is empty")
    # this was originally checked with:
    #
    # sudo -u git git -C $repo.git rev-list -n 1 --all | grep -q .
    # sudo -u git git -C $repo.git count-objects -v | grep -e 'count: [^0]' -e 'packs: [^0]'
    #
    # the rev-list was considered redundant because revs *would* show up as objects
    return non_empty_stats


# original command:
#
# find /var/opt/gitlab/git-data/repositories/@hashed \
#    -type d -name \*.git -not -name \*.wiki.git \
#    -exec bash -c "test -e {}/HEAD &&
#                   test -n \$(sudo -u git git -C {} rev-list -n1 --all) &&
#                   sudo -u git git -C {} grep :bullseye HEAD -- .gitlab-ci.yml &&
#                   echo {}" \;
#
# a few optimisations:
#
# - we don't skip wikis, since we might want those anyways
#
# - we don't check for HEAD or rev-list, and assume git grep will
#   quickly discard those, instead we ignore errors it generates about
#   missing HEADs
@task
def grep_all_repos(
    con,
    pattern: str,
    files: str | None = None,
    repos_path="/var/opt/gitlab/git-data/repositories/@hashed",
    list_only: bool = False,
):
    """grep for a specific pattern in a bunch of git repositories

    This will run the `git-grep` command on multiple repos, assumed to
    be bare repositories under the REPOS_PATH parameter. If no FILES
    is given, it will grep all files in each repo, otherwise it will
    restrict to the named FILES.

    By default, it will output the matches, but if LIST_ONLY is
    provided, it will only provide the matching *repositories* with a
    match (as opposed to files inside the repositories.

    The normal output replaces the normal git-grep output which would
    show HEAD:MATCH by PATH:MATCH.
    """

    # we don't reuse list_all_repos here because it would be too slow
    # to do the roundtrip for each matching repo.
    if list_only:
        cmd = r"find %s -type d -a -name '*.git' -exec sh -c 'git -C {} grep -q %s HEAD %s 2>/dev/null && echo {}' \;"
    else:
        cmd = r"find %s -type d -a -name '*.git' -exec sh -c 'git -C {} grep %s HEAD %s 2>/dev/null | sed s,^HEAD:,{}:, ' \;"  # noqa: E501

    cmd = cmd % (
        repos_path,
        pattern,
        " -- %s" % files if files else "",
    )
    logging.info("running %s on %s", cmd, getattr(con, "host", "localhost"))
    ret = con.run(cmd, warn=True)
    if not ret.ok:
        logging.info("find(1) command failed")
