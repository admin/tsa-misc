#!/usr/bin/python3
# coding: utf-8

"""installation procedures"""

# Copyright (C) 2020-2024 Antoine Beaupré <anarcat@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import ipaddress
import logging
import os.path
from pathlib import Path
import sys


try:
    from fabric import task
except ImportError:
    sys.stderr.write(
        "cannot find fabric, install with `apt install python3-fabric`"
    )  # noqa: E501
    raise
from invoke import Context
import invoke.exceptions

from .ui import no_yes
from . import silence
from .host import create_swapfile, write_to_file, ifconfig, rewrite_interfaces_ifconfig


@task
def hetzner_robot(
    con,
    fqdn,
    fai_disk_config,
    package_list,
    post_scripts_dir,
    mirror="https://deb.debian.org/debian/",
    ipv4_address=None,
    ipv4_subnet="24",
    ipv4_gateway=None,
    ipv6_address=None,
    ipv6_subnet="24",
    ipv6_gateway=None,
    console_idx=0,
    swap_size="1GiB",
):
    """install a new hetzner server

    As an exception, the `--hosts` (`-H`) argument *must* be the IP
    address here. The actual hostname, provided as an argument, will
    be *set* on the host.
    """
    # TODO: boot_disk should be a list?

    # TODO: automatically guess package_list and post_scripts_dir
    # based on current path

    # summary of the new-machine-hetzner-robot procedure:
    #
    # STEP 1: login over SSH, checking fingerprint
    # STEP 2: set hostname
    # STEP 3: partition disks
    # STEP 4: run grml-debootstrap with packages and post-scripts
    # STEP 5: setup dropbear-initramfs (in the post-scripts)
    # STEP 6: crypto config
    # STEP 7: network config
    # STEP 8: regenerate initramfs if relevant
    # STEP 9: unmount
    # STEP 10: close volumes
    # STEP 11: document root password
    # STEP 12: reboot

    # STEP 1: logging is handled in the `install` script, in the
    # MatchingHostKeyPolicy class
    logging.info("STEP 1: fingerprint checked, making sure we have access")
    con.run("hostname ; uptime")

    hostname, _ = fqdn.split(".", 1)
    if not no_yes("ready to wipe system and automate install of %s?" % hostname):
        raise invoke.exceptions.Exit("canceled")

    logging.info("STEP 2: setting hostname to %s", hostname)
    if con.run("hostname %s" % hostname, warn=True).failed:
        logging.error("failed to set hostname, aborting")
        return False

    sftp = con.sftp()

    # keep trailing slash
    remote_conf_path = "/etc/tpa-installer/"
    try:
        sftp.mkdir(remote_conf_path)
    except OSError as e:
        # ignore existing directory
        #
        # XXX: SFTP doesn't really help us distinguish between real
        # and "EEXIST" errors, it just returns an error code 4
        # ("SSH_FX_FAILURE") if the directory exists
        if "Failure" in str(e):
            pass

    logging.info("STEP 3: partition disks")
    fai_disk_config_remote = remote_conf_path + os.path.basename(fai_disk_config)
    logging.info(
        "deploying disk config %s to %s", fai_disk_config, fai_disk_config_remote
    )
    con.put(fai_disk_config, remote=fai_disk_config_remote)

    logging.info("installing fai-setup-storage(8)")
    if con.run(
        "apt update && apt install -y fai-setup-storage cryptsetup", warn=True
    ).failed:
        logging.error("failed to install fai-setup-storage and cryptsetup, aborting")
        return False

    # the rationale here is that some of the dependencies we need
    # might have security vulnerabilities, and i have found the rescue
    # images sometimes don't have the latest
    logging.info("running upgrades")
    # we just ignore errors on this one, as we can afford to live
    # without latest, we tried.
    con.run("apt upgrade -yy", warn=True)

    has_efi = not con.run("[ -d /sys/firmware/efi ]", warn=True).failed
    if has_efi:
        logging.info("EFI system detected")
    else:
        logging.info("legacy BIOS system detected")

    command = "setup-storage -f '%s' -X" % fai_disk_config_remote
    logging.info("launching %s", command)
    if con.run(command, warn=True).failed:
        logging.error("partitioning failed, aborting install")
        return False

    logging.info("/tmp/fai/disk_var.sh contents: ")
    con.run("cat /tmp/fai/disk_var.sh", warn=True)
    # TODO: parse the .sh file ourselves?
    #
    # content of a disk_var.sh:
    #
    # BOOT_DEVICE=${BOOT_DEVICE:-"/dev/sda"}
    # ROOT_PARTITION=${ROOT_PARTITION:-UUID=2da10834-8f4f-412f-901e-a6d9d77eb9ac}
    # SWAPLIST=${SWAPLIST:-""}
    # BOOT_PARTITION=${BOOT_PARTITION:-/dev/sda2}
    # PHYSICAL_BOOT_DEVICES="/dev/sda"
    #
    # the above is a single-disk, plaintext, legacy bios install (grub-pc)

    # TODO: test if we can skip that test by passing `$ROOT_PARTITION`
    # as a `--target` to `grml-debootstrap`. Probably not.
    logging.info("mounting partitions from FAI")
    res = con.run(
        '. /tmp/fai/disk_var.sh && mkdir -p /target && mount "$ROOT_PARTITION" /target',
        warn=True,
    )
    if res.failed:
        logging.error("failed to mount /target prior to debbootstrap, aborting")
        return False

    logging.info("checking if boot device is already mounted")

    res = con.run(
        """. /tmp/fai/disk_var.sh &&
        mkdir -p /target/boot &&
        mount "${BOOT_PARTITION:-$BOOT_DEVICE}" /target/boot""",  # noqa: E501
        warn=True,
    )
    if res.failed:
        if "already mounted" in res.stdout:
            logging.info("already mounted")
        else:
            logging.warning("mount command failed for some unknown error, continuing")
    else:
        logging.info("boot device mounted")

    logging.info("STEP 4: setting up grml-debootstrap")
    logging.info("uploading package list %s", package_list)
    package_list_remote = remote_conf_path + os.path.basename(package_list)
    con.put(package_list, remote=package_list_remote)

    # TODO: those post-scripts *could* be turned into one nice fabric
    # recipe instead, after all we still have access to the chroot
    # after and know what we need to do at the end (ie. rebuild
    # initramfs and install grub)
    post_scripts_dir_remote = remote_conf_path + "post-scripts/"
    logging.info(
        "uploading post-scripts %s to %s", post_scripts_dir, post_scripts_dir_remote
    )
    try:
        sftp.mkdir(post_scripts_dir_remote)
    except OSError as e:
        # ignore existing directory, see earlier XXX
        if "Failure" in str(e):
            pass
    for post_script in Path(post_scripts_dir).iterdir():
        filename = str(post_script.resolve())
        remote = post_scripts_dir_remote + os.path.basename(filename)
        logging.debug("uploading %s to %s", filename, remote)
        con.put(filename, remote=remote)

    logging.info("installing grml-debootstrap")
    # XXX: error handling?
    con.run("apt-get install -y grml-debootstrap")

    logging.info("setting up grml-debootstrap")
    # XXX: error handling?
    con.run(
        """mkdir -p /target/run && \
        mount -t tmpfs tgt-run /target/run && \
        mkdir /target/run/udev && \
        mount -o bind /run/udev /target/run/udev"""
    )

    # TODO: do we really need grml-deboostrap here? why not just use
    # plain debootstrap?
    #
    # grml-debootstrap does this on top of debootstrap for us:
    #
    # 1. running hooks inside the chroot
    # 2. the grub install (with EFI support)
    # 3. create a /etc/network/interfaces file
    #
    # the grub install is particularly tricky, grml-debootstrap has
    # all sorts of tweaks to support writing to an image, mounting the
    # right filesystems, EFI, etc
    #
    # it seems this could be as simple as:
    #
    # apt install grub-efi-amd64 # or grub-pc for legacy
    # grub-install --boot-directory=/boot --bootloader-id=Debian --target=x86_64-efi --efi-directory=/boot/efi --recheck
    # efibootmgr -c -g -d /dev/sda -p 3 -L Debian -l '\EFI\Debian\grubx64.efi'
    # update-grub
    #
    # A/I decided whether to use EFI or not based on the existence of
    # /sys/firmware/efi. they also use --metadata=0.90 for RAID on
    # that partition, if they do use RAID. grml-debootstrap doesn't do
    # the efibootmgr step and do not run grub-install themselves, it
    # seems they rely on the package doing the right thing there. it's
    # important to bind-mount /run/udev in either case (#918590)
    #
    # there is also cool stuff it supports, but which we do not use:
    #
    # 1. --ssh-copyid and --ssh-copy-auth (using our own wrapper instead)
    # 2. do some smart guesses on interface names (in preparechroot,
    # grep for udevadm)
    # 3. support creating an ISO, building RAID arrays, filesystems
    # 4. interactive mode
    #
    # another thing from mikap @grml, this is how they do second RAID
    # disks:
    # https://github.com/sipwise/deployment-iso/blob/1b1e54b822b8af6b6c691993eae9d6589ed8b483/templates/scripts/includes/deployment.sh#L2175
    installer = """. /tmp/fai/disk_var.sh && \
        AUTOINSTALL=y grml-debootstrap \
            --target /target \
            --hostname `hostname` \
            --release bookworm \
            --mirror "%s" \
            --packages %s \
            --post-scripts %s \
            --nopassword \
            --remove-configs \
            --defaultinterfaces""" % (
        mirror,
        package_list_remote,
        post_scripts_dir_remote,
    )
    if has_efi:
        installer += ' --efi "$ESP_DEVICE"'
    installer += ' --grub "$BOOT_DEVICE"'
    logging.info("STEP 4: installing system with grml-debootstrap")
    logging.info("main installer command: %s", installer)
    con.run(installer)
    logging.info("grml-debootstrap install completed, post-install fabric tasks")

    create_swapfile(
        con, path="/target/swapfile", size=swap_size, fstab="/target/etc/fstab"
    )

    # TODO: extract the resulting SSH keys and inject in a local
    # known_hosts for further bootstrapping. e.g.:
    logging.info("dumping host SSH keys")
    # XXX: error handling?
    con.run("cat /target/etc/ssh/ssh_host_*.pub")
    # XXX: this is incredibly ugly - maybe list the files with sftp
    # and then call the chroot?
    logging.info("dumping dropbear SSH keys")
    con.run(
        "for key in "
        " $(echo /target/etc/dropbear-initramfs/dropbear_*_host_key  "
        """| sed "s#/target##g") """
        " $(echo /target/etc/dropbear/initramfs/dropbear_*_host_key "
        """| sed "s#/target##g") """
        "; do "
        "    chroot /target dropbearkey -y -f $key;    "
        "done"
    )

    # okay, this is officially a mess
    #
    # we should not be remounting here. instead those should be hooks
    # that run in grml-debootstrap. *buuut* those don't have access to
    # the information that's passed around here, like the IP address
    # and so on.
    #
    # the proper way to go is probably to stop using grml-debootstrap
    # and do the mounting and grub install ourselves, since we end up
    # doing it anyways in the hooks.
    #
    # remounting /target seems necessary here otherwise the `chmod 0`
    # below fails
    logging.info("remounting /target read-write")
    con.run("mount -o remount,rw /target")

    logging.info("configuring grub serial console")
    grub_serial = f"""# enable kernel's serial console on port 1 (or 0, if you count from there)
GRUB_CMDLINE_LINUX="$GRUB_CMDLINE_LINUX console=tty0 console=ttyS{console_idx},115200n8"
# same with grub itself
GRUB_TERMINAL="serial console"
GRUB_SERIAL_COMMAND="serial --speed=115200 --unit={console_idx} --word=8 --parity=no --stop=1"
"""
    write_to_file(
        con, "/target/etc/default/grub.d/serial.cfg", grub_serial.encode("ascii")
    )

    logging.info(
        'disabling "predictable" interface names, that is: setting net.ifnames=0'
    )
    write_to_file(
        con,
        "/target/etc/default/grub.d/local-ifnames.cfg",
        'GRUB_CMDLINE_LINUX="$GRUB_CMDLINE_LINUX net.ifnames=0"'.encode("ascii"),
    )

    logging.info("STEP 5: locking down /target/etc/luks")
    # already handled in 50-tor-install-luks-setup
    if con.run("chmod 0 /target/etc/luks/", warn=True).failed:
        logging.warning(
            "W: failed to lock down /target/etc/luks, probably harmless as an earlier hook is supposed to fix this"
        )

    logging.info("STEP 6: verifying /target/etc/crypttab")
    if con.run("cat /target/etc/crypttab", warn=True).failed:
        logging.warning("W: no /target/etc/crypttab? continuing anyway")

    if ipv4_address:
        logging.info("STEP 7: network config, writing /target/etc/network/interfaces")
        ipconf = ifconfig(
            ipv4_address,
            ipv4_subnet,
            ipv4_gateway,
            ipv6_address,
            ipv6_subnet,
            ipv6_gateway,
        )
        rewrite_interfaces_ifconfig(con, ipconf, "/target/etc/network/interfaces")

        logging.info("configuring dropbear-initramfs in grub")
        ipv4_netmask = str(
            ipaddress.IPv4Interface(f"{ipv4_address}/{ipv4_subnet}").netmask
        )
        grub_dropbear = f"""# for dropbear-initramfs because we don't have dhcp
GRUB_CMDLINE_LINUX="$GRUB_CMDLINE_LINUX ip={ipv4_address}::{ipv4_gateway}:{ipv4_netmask}::eth0:off"
""".encode(
            "ascii"
        )
        write_to_file(
            con, "/target/etc/default/grub.d/local-ipaddress.cfg", grub_dropbear
        )
    else:
        logging.warning(
            "STEP 7: network configuration skipped, no IPv4 information provided"
        )

    logging.info("contents of /target/etc/network/interfaces")
    con.run("cat /target/etc/network/interfaces", warn=True)

    logging.info("grub configurations in /target/etc/default/grub.d/*.cfg")
    con.run("cat /target/etc/default/grub.d/*.cfg", warn=True)

    # necessary because we changed the serial config too
    logging.info("STEP 8: rebuild initramfs and grub")
    logging.info("checking if boot device is already mounted")

    res = con.run(
        """. /tmp/fai/disk_var.sh &&
        mkdir -p /target/boot &&
        mount "${BOOT_PARTITION:-$BOOT_DEVICE}" /target/boot""",  # noqa: E501
        warn=True,
    )
    if res.failed:
        if "already mounted" in res.stdout:
            logging.info("already mounted")
        else:
            logging.warning("mount command failed for some unknown error, continuing")
    else:
        logging.info("boot device mounted")
    con.run("for fs in dev proc run sys  ; do mount -o bind /$fs /target/$fs; done")
    if has_efi:
        con.run(
            """. /tmp/fai/disk_var.sh && \
            mkdir -p /target/boot/efi && mount $ESP_DEVICE /target/boot/efi &&
            mount -t efivarfs efivarfs /target/sys/firmware/efi/efivars"""
        )

    con.run("chroot /target sh -c '/usr/share/mdadm/mkconf > /etc/mdadm/mdadm.conf'")
    con.run("chroot /target update-initramfs -u")
    con.run("chroot /target update-grub")

    # reinstall grub
    con.run(
        """. /tmp/fai/disk_var.sh && \
            for DEVICE in $PHYSICAL_BOOT_DEVICES ; do chroot /target grub-install $DEVICE ; done"""
    )
    logging.info("STEP 9: unmount everything")
    if has_efi:
        con.run("umount /target/sys/firmware/efi/efivars", warn=True)
    con.run("umount /target/run/udev", warn=True)
    con.run(
        "for fs in dev proc run sys  ; do mount -o bind /$fs /target/$fs; done",
        warn=True,
    )
    con.run("umount /target/boot/efi /target/boot /target", warn=True)
    con.run("rmdir /target", warn=True)

    logging.info("STEP 10: close volume groups, LUKS and stop RAID")
    # we ignore errors here and below, reboot will clean all of this anyway
    con.run("vgchange -a n", warn=True)
    # HACK: we're doing a wild guess here...
    con.run("cryptsetup luksClose /dev/mapper/crypt_dev_*", warn=True)
    con.run("mdadm --stop /dev/md*", warn=True)

    logging.info("STEP 11: document LUKS and root password in pwmanager (TODO)")
    logging.info("STEP 12: reboot (TODO)")
    # XXX: error handling?
    logging.info(
        'reboot machine when happy: ./reboot -H root@%s -v --reason "new-machine procedure" --delay-shutdown 0'
        % con.host
    )  # noqa: E501

    silence.create(
        Context(),
        [
            f"alias={fqdn}",
            "job=bacula",
        ],
        "silencing backup alerts until host runs its first backup",
        ends_at="in 2 days",
    )

    # STEP X: reverse DNS, irl has an Ansible manifest for this which
    # we could reuse: https://iain.learmonth.me/blog/2020/2020w187/
