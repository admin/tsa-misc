#!/usr/bin/python3
# coding: utf-8

"""Prometheus glue code"""
# Copyright (C) 2024 Antoine Beaupré <anarcat@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

import json
import logging
import os.path
from typing import Iterable, Iterator

from fabric import task, Connection
from invoke.context import Context
import requests


DEFAULT_API_URL = "https://prometheus.torproject.org/api/v1"


@task
def query(
    _: Context,
    expression: str,
    json_output: bool = True,
    limit: int = 0,
    http_user: str = "tor-guest",
    http_password: str = "",
    api_url: str = DEFAULT_API_URL,
) -> None:
    """Query prometheus and display the results as JSON, or a list of hosts.

    Note that your query is sent directly to the prometheus API URL so you don't
    need to run this task against a remote host.

    If you use --no-json-output, the output will be only the `alias` field in
    the query, one per line.
    """
    if not json_output:
        print(
            "\n".join(
                hosts_from_query(
                    expression,
                    limit=limit,
                    http_user=http_user,
                    http_password=http_password,
                    api_url=api_url,
                )
            )
        )
    else:
        print(
            json.dumps(
                query_api(
                    expression,
                    limit=limit,
                    http_user=http_user,
                    http_password=http_password,
                    api_url=api_url,
                )
            )
        )


def query_api(
    expression: str,
    start: str | None = None,
    end: str | None = None,
    limit: int = 0,
    step: str | None = None,
    http_user: str = "tor-guest",
    http_password: str = "",
    api_url: str = DEFAULT_API_URL,
) -> dict:
    """boilerplate code to send a query to a Prometheus server

    This returns the JSON blob from the server directly, but will log
    an error if detected. Called should check that the returned
    object's "status" key is not "error".
    """
    if start is None and end is None:
        url = os.path.join(api_url, "query")
    elif start is not None and end is not None:
        if not step:
            logging.error(
                "step must be specified, e.g. 15s, "
                "see https://prometheus.io/docs/prometheus/latest/querying/api/#range-queries"
            )
            return {}
        url = os.path.join(api_url, "query_range")
    else:
        logging.error("start and end need to be specified together or not at all")
        return {}

    logging.debug("sending query %s to %s", expression, url)
    # equivalent shell command for instant queries:
    # curl -sSL --data-urlencode query="$query" \
    # 'https://$HTTP_USER@prometheus.torproject.org/api/v1/query \

    params = {"query": expression, "limit": str(limit)}
    if start is not None:
        assert end
        assert step
        params |= {
            "start": start,
            "end": end,
            "step": step,
        }

    result = requests.get(
        url,
        auth=(http_user, http_password),
        params=params,
    )
    dump = result.json()
    logging.debug("query dump: %r", result.text)
    assert dump.get("status") is not None, "malformed Prometheus API response"
    if dump.get("status") == "error":
        logging.error("error in Prometheus query: %s", dump.get("error"))
    return dump


def hosts_from_query(
    expression: str,
    http_user: str = "tor-guest",
    http_password: str = "",
    api_url: str = DEFAULT_API_URL,
    sample_query: str = "",
    exclude_hosts: Iterable[str] = ["dal-rescue-02.torproject.org"],
    limit: int = 0,
) -> list[str]:
    dump = query_api(
        expression,
        http_user=http_user,
        http_password=http_password,
        api_url=api_url,
    )
    all_hosts = set()
    for metric in dump.get("data", {}).get("result", []):
        alias = metric.get("metric", {}).get("alias")
        all_hosts.add(alias)
    exclude_hosts_set = set(exclude_hosts)
    if exclude_hosts_set.issubset(all_hosts):
        # workaround: fabric has trouble with hosts that have a
        # port number like [dal-rescue-02.torproject.org]:4622
        logging.warning(
            "skipping over hosts %s",
            " ".join(exclude_hosts_set.intersection(all_hosts)),
        )
        all_hosts.difference_update(exclude_hosts_set)
    if not all_hosts:
        logging.info("found no hosts with Prometheus query: '%s'", expression)
        if sample_query:
            logging.info(
                "consider a broader query to inspect all hosts, for example: '%s'",
                sample_query,
            )
        return []
    all_hosts_sorted = sorted(all_hosts)
    logging.info(
        "found %d hosts matching query %s: %s",
        len(all_hosts_sorted),
        expression,
        " ".join(all_hosts_sorted),
    )
    if limit:
        return all_hosts_sorted[:limit]
    return all_hosts_sorted


@task
def deploy(con: Connection):
    """deploy pending changes to Prometheus server

    This duplicates the work done by Puppet on a regular basis, but is
    present here to speed things up, as Puppet can be slow on this.

    This should be kept in sync with the vcsrepo resources in
    profile::prometheus::server::internal and profile::grafana.
    """
    logging.info("Pulling prometheus alerts repository")
    con.run("git -C /etc/prometheus-alerts pull")
    logging.info("Reloading Prometheus")
    con.run("systemctl reload-or-restart prometheus")
    logging.info("Pulling Grafana dashboard")
    con.run("sudo -u grafana git -C /var/lib/grafana/dashboards pull")
    logging.info("Reloading Grafana")
    con.run("systemctl reload-or-restart grafana-server")
    logging.info("Running Puppet to make sure we have the latest exported resources")
    logging.info("You're probably mostly done, you can carry on while this runs.")
    con.run("patc")


def metric_dict_to_str(metric_labels: dict[str, str]) -> str:
    assert metric_labels.get("__name__")
    metric_string = metric_labels["__name__"]  # "up"
    del metric_labels["__name__"]
    if metric_labels:
        metric_string += (
            "{" + ",".join([f'{k}="{v}"' for k, v in metric_labels.items()]) + "}"
        )
    return metric_string


def _parse_instant_results(
    metrics: list[dict[str, dict | list]],
) -> Iterator[tuple[str, str]]:
    for metric in metrics:
        metric_labels = metric.get("metric", {})
        assert isinstance(metric_labels, dict)
        metric_string = metric_dict_to_str(metric_labels)

        assert len(metric.get("value", [])) == 2
        timestamp, value = metric["value"]
        yield metric_string, value


def _parse_range_results(
    metrics: list[dict[str, dict | list[list]]],
) -> Iterator[tuple[str, str, str]]:
    for metric in metrics:
        metric_labels = metric.get("metric", {})
        assert isinstance(metric_labels, dict)
        metric_string = metric_dict_to_str(metric_labels)
        logging.debug("metric: %s, value: %s", metric_string, metric.get("values", []))
        for ts, value in metric.get("values", []):
            yield metric_string, ts, value


def _merge_repeats(series: str | Iterable[str]) -> str:
    """merge repeated values in a way usable by alertmanager's unit tests

    >>> _merge_repeats('1 2 3'.split())
    '1 2 3'
    >>> _merge_repeats('1 1 2 2 3'.split())
    '1x2 2x2 3'
    >>> _merge_repeats(['1' for _ in range(10)])
    '1x10'
    >>> _merge_repeats([1, 1, 1])
    '1x3'
    >>> _merge_repeats([1, 2, 3])
    '1 2 3'

    Note that this silently accepts non-string inputs, as long as they
    are comparable and not None. Everything is cast to string before
    being returned.
    """
    result = []
    prev = None
    count = 0
    for i in series:
        if prev is None:
            prev = i
            count += 1
            continue
        if prev == i:
            count += 1
            continue
        # prev != i
        if count > 0:
            if count > 1:
                result.append(f"{prev}x{count}")
            else:
                result.append(prev)
            prev = i
            count = 1

    if count > 1:
        result.append(f"{i}x{count}")
    else:
        result.append(i)
    return " ".join([str(x) for x in result])


@task
def query_to_series(
    _: Context,
    expression: str,
    start: str | None = None,
    end: str | None = None,
    limit: int = 0,
    step: str = "15s",
    coalesce: bool = False,
    http_user: str = "tor-guest",
    http_password: str = "",
    api_url: str = DEFAULT_API_URL,
):
    dump = query_api(
        expression,
        start=start,
        end=end,
        limit=limit,
        step=step,
        http_user=http_user,
        http_password=http_password,
        api_url=api_url,
    )
    if start:
        # range queries
        if coalesce:
            series: dict[str, list] = {}
            for metric, ts, value in sorted(
                _parse_range_results(dump.get("data", {}).get("result", []))
            ):
                if metric not in series:
                    series[metric] = []
                series[metric].append(value)
            for metric, values in series.items():
                print(f"- series: '{metric}'")
                print("  values: " + _merge_repeats(values))
        else:
            for metric, ts, value in _parse_range_results(
                dump.get("data", {}).get("result", [])
            ):
                print(metric, value, f"@[{ts}]")
    else:
        for metric, value in _parse_instant_results(
            dump.get("data", {}).get("result", [])
        ):
            print(metric, value)
