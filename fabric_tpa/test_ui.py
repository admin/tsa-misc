from unittest.mock import patch
import pytest

from fabric_tpa.ui import yes_no, no_yes, pick

test_yes_no_answers = [
    ("y", True),
    ("n", False),
    ("", True),
]


@pytest.mark.parametrize("answer,expected", test_yes_no_answers)
def test_yes_no(answer, expected):
    with patch("builtins.input", return_value=answer):
        assert yes_no("prompt") is expected


test_no_yes_answers = [
    ("y", True),
    ("n", False),
    ("", False),
]


@pytest.mark.parametrize("answer,expected", test_no_yes_answers)
def test_no_yes(answer, expected):
    with patch("builtins.input", return_value=answer):
        assert no_yes("prompt") is expected


def test_pick():
    options = "abcd"
    with patch("builtins.input", return_value=""):
        assert pick(options) is None

    with patch("builtins.input", return_value="a"):
        assert pick(options) == "a"

    with patch("builtins.input", return_value="1"):
        assert pick(options) == "b"

    assert pick("a") == "a"
    assert pick("b") == "b"

    assert pick("") is None
