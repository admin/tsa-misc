import io

from pytest import raises
from . import host


def test_parse_rewrite_interfaces_diff():
    diff = """--- /mnt/etc/network/interfaces.bak	2020-04-01 21:11:20.501228991 +0000
+++ /mnt/etc/network/interfaces	2020-04-01 21:11:21.701219470 +0000
@@ -1,11 +1,16 @@
+# This file describes the network interfaces available on your system
+# and how to activate them. For more information, see interfaces(5).
+
+# The loopback network interface
 auto lo
 iface lo inet loopback

-allow-hotplug eth0
+# The primary network interface
+auto eth0
 iface eth0 inet static
-    address 138.201.212.227/28
-    gateway 138.201.212.225
+    address 116.202.120.188/27
+    gateway 116.202.120.161
 iface eth0 inet6 static
     accept_ra 0
-    address 2a01:4f8:172:39ca:0:dad3:3:1/96
-    gateway 2a01:4f8:172:39ca:0:dad3:0:1
+    address 2a01:4f8:fff0:4f:266:37ff:fe80:b04/64
+    gateway 2a01:4f8:fff0:4f::1
"""
    assert (
        "138.201.212.227",
        "2a01:4f8:172:39ca:0:dad3:3:1",
    ) == host.parse_rewrite_interfaces_diff(diff)


def test_parse_apt_cache_policy_stream():
    example_data = """qml-module-org-kde-kirigami2:
  Installed: 5.103.0-1
  Candidate: 5.103.0-1
  Version table:
     5.115.0-2+b1 1
          1 https://deb.debian.org/debian sid/main amd64 Packages
          1 https://deb.debian.org/debian trixie/main amd64 Packages
 *** 5.103.0-1 500
        500 https://deb.debian.org/debian bookworm/main amd64 Packages
        100 /var/lib/dpkg/status
elpa-puppet-mode:
  Installed: 0.4-2
  Candidate: 0.4-3
  Version table:
     0.4-5 1
          1 https://deb.debian.org/debian sid/main amd64 Packages
          1 https://deb.debian.org/debian trixie/main amd64 Packages
 *** 0.4-2 500
        500 https://deb.debian.org/debian bookworm/main amd64 Packages
        100 /var/lib/dpkg/status"""
    assert list(host._parse_apt_cache_policy_stream(io.StringIO(example_data))) == [
        host.AptPackageAvailability(
            "qml-module-org-kde-kirigami2", "5.103.0-1", "5.103.0-1"
        ),
        host.AptPackageAvailability("elpa-puppet-mode", "0.4-2", "0.4-3"),
    ]

    # this use case, mysteriously, doesn't show up in the wild,
    # possibly because apt-cache is silent when ran from fabric?
    example_failure = "N: Unable to locate package ~i\n"
    stream = io.StringIO(example_failure)
    with raises(host.AptParsingError, match="unable to extract package list.*"):
        list(host._parse_apt_cache_policy_stream(stream))
