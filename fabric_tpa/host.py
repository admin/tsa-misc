#!/usr/bin/python3
# coding: utf-8

"""host-specific fabric tasks"""

# Copyright (C) 2020-2024 Antoine Beaupré <anarcat@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import division, absolute_import
from __future__ import print_function, unicode_literals

from collections import namedtuple
from contextlib import contextmanager
from dataclasses import dataclass
import io
import logging
import re
import secrets
import socket
import subprocess
import sys
from typing import IO, Iterable, Iterator


try:
    from fabric import task, Connection, Result
except ImportError:
    sys.stderr.write(
        "cannot find fabric, install with `apt install python3-fabric`"
    )  # noqa: E501
    raise
import invoke.exceptions

from .ldap import LdapContext


# time after which data is removed when a host is retired, passed to
# `at(1)` in `schedule_job()`.
DEFAULT_RETIREMENT_DELAY_VM = 7
DEFAULT_RETIREMENT_DELAY_BACKUPS = 30


@task
def id(con):
    """run the `id` command through fabric

    Mostly for connection debugging purposes"""
    return con.run("id")


@task
def path_exists(host_con, path):
    """check if path exist with SFTP"""
    logging.info('checking for path "%s" on %s', path, host_con.host)
    sftp = host_con.sftp()
    try:
        sftp.chdir(path)
    except IOError as e:
        logging.error("path %s not found: %s", path, e)
        return False
    return True


@task
def schedule_delete(host_con, path, delay):
    """schedule removal of PATH in the future

    The job is scheduled with `at(1)` so the DELAY is interpreted
    accordingly. Normally, it should be something like "7 days".
    """

    # TODO: shell escapes?
    command = 'rm -rf "%s"' % path
    return schedule_job(host_con, command, delay)


@task
def schedule_job(host_con, command, delay):
    logging.info("scheduling %s to run on %s in %s", command, host_con.host, delay)
    return host_con.run("echo '%s' | at now + %s" % (command, delay), warn=True).ok


@task(autoprint=True)
def fetch_ssh_host_pubkey(con, type="ed25519"):
    """fetch public host key from server"""
    buffer = io.BytesIO()
    pubkey_path = "/etc/ssh/ssh_host_%s_key.pub" % type
    try:
        con.get(pubkey_path, local=buffer)
    except OSError as e:
        logging.error("cannot fetch instance config from %s: %s", pubkey_path, e)
        return False
    return buffer.getvalue()


@task
def write_to_file(con, path, content, mode="wb"):
    _write_to_file(con, path, content, mode=mode)


def _write_to_file(con, path, content, mode="wb"):
    """append bytes to a file

    This does not check for duplicates."""
    if con.config.run.dry:
        return
    if getattr(con, "sftp"):
        with con.sftp().file(path, mode=mode) as fp:
            fp.write(content)
    else:
        with open(path, mode=mode) as fp:
            fp.write(content)


@task
def ensure_line(con, path, line, match=None, ensure_newline=True):
    if con.config.run.dry:
        return
    if getattr(con, "sftp", None):
        with con.sftp().file(path, mode="ab+") as fp:
            ensure_line_stream(fp, line, match=match, ensure_newline=ensure_newline)
    else:
        with open(path, mode="ab+") as fp:
            ensure_line_stream(fp, line, match=match, ensure_newline=ensure_newline)


def ensure_line_stream(
    stream, line, match=None, ensure_newline=True, flags=re.MULTILINE
):
    """ensure that line is present in the given stream, adding it if missing

    Will ensure the given line is present in the stream. If match is
    provided, it's treated as a regular expression for a pattern to
    look for. It will *not* replace the line matching the pattern,
    just look for it. If match is not provided, it defaults to the
    full line on its own line.

    If ensure_newline is specified (the default), it will also append a
    newline character even if missing from the line.

    This is inspired by Puppet's stdlib file_line resource:

    https://github.com/puppetlabs/puppetlabs-stdlib/"""
    if match is None:
        match = b"^" + re.escape(line) + b"$"
    rep = re.compile(match, flags=flags)
    stream.seek(0)
    # TODO: loads entire file in memory, could be optimized
    content = stream.read()
    res = rep.search(content)
    if res:
        if res.group(0).strip() == line.strip():
            logging.debug("exact line present in stream %s, skipping: %s", stream, line)
        else:
            logging.debug(
                "match found in stream %s: %s; replacing with %s",
                stream,
                res.group(0),
                line,
            )
            stream.seek(0)
            content_new = rep.sub(line, content)
            logging.debug("before: %s; after: %s", content, content_new)
            stream.seek(0)
            stream.truncate(0)
            stream.write(content_new)
    else:
        logging.debug("line not found in stream %s, appending: %s", stream, line)
        stream.seek(0, 2)  # EOF
        stream.write(line)

        # append newline only if the line is totally missing, because
        # otherwise if we replace an existing line the regex doesn't
        # match the newline (unless the caller passes flags=DOTALL, in
        # which case they are responsible for the trouble they're
        # in. see ensure_ssh_key_stream() for what "trouble" looks
        # like.
        if ensure_newline and not line.endswith(b"\n"):
            stream.write(b"\n")
    return stream


def test_ensure_line_stream():
    """test for ensure_line_stream"""
    import io

    stream = io.BytesIO()
    ensure_line_stream(stream, b"// test", ensure_newline=False)
    assert stream.seek(0) == 0
    assert stream.read() == b"// test", "appends if empty, without newline"

    stream = io.BytesIO()
    ensure_line_stream(stream, b"// test")
    assert stream.seek(0) == 0
    assert stream.read() == b"// test\n", "appends if empty, with newline"
    ensure_line_stream(stream, b"test")
    stream.seek(0)
    assert stream.read() == b"// test\ntest\n", "appends if not full match"

    stream = io.BytesIO(b"// test\n")
    ensure_line_stream(stream, b"// test", match=b"^.*test.*$")
    stream.seek(0)
    assert stream.read() == b"// test\n", "does not append on partial"

    ensure_line_stream(stream, b"test", match=b"^.*test.*$")
    stream.seek(0)
    assert stream.read() == b"test\n", "replaces on partial"

    header = b"""# HEADER: This file was autogenerated at 2020-03-24 22:35:16 +0000
# HEADER: by puppet.  While it can still be managed manually, it
# HEADER: is definitely not recommended.
"""
    key = b"ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIyMtFIII1NEokPxlDfa6Sx2evW3+xvtzziso81aVqJf root@fsn-node-05.torproject.org"  # noqa: E501
    key_comment = b"# fsn-node-05.torproject.org pubkey for forrestii.torproject.org transfer"  # noqa: E501

    key_blob = key_comment + b"\n" + key

    stream = io.BytesIO(header)
    ensure_line_stream(
        stream, key_blob, match=rb"(?:^#[^\n]*$)?\s+" + re.escape(key) + b"$"
    )
    stream.seek(0)
    assert stream.read() == header + key_blob + b"\n"
    ensure_line_stream(
        stream, key_blob, match=rb"(?:^#[^\n]*$)?\s+" + re.escape(key) + b"$"
    )
    stream.seek(0)
    assert stream.read() == header + key_blob + b"\n"


@task
def ensure_ssh_key(con, path, key, comment=None):
    if con.config.run.dry:
        return
    with con.sftp().file(path, mode="ab+") as fp:
        ensure_ssh_key_stream(fp, key, comment)


def ensure_ssh_key_stream(stream, key, comment=None):
    if comment is None:
        match = None
        key_blob = key
    else:
        match = rb"(?:^#[^\n]*$)?\s+" + re.escape(key) + b"$"
        key_blob = comment + b"\n" + key
    ensure_line_stream(stream, key_blob, match=match, flags=re.MULTILINE | re.DOTALL)


def test_ensure_ssh_key():
    header = b"""# HEADER: This file was autogenerated at 2020-03-24 22:35:16 +0000
# HEADER: by puppet.  While it can still be managed manually, it
# HEADER: is definitely not recommended.
# This is some sample key that should be preserved
ssh-rsa SOME KEY
"""
    key = b"ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIIyMtFIII1NEokPxlDfa6Sx2evW3+xvtzziso81aVqJf root@fsn-node-05.torproject.org"  # noqa: E501
    key_comment = b"# fsn-node-05.torproject.org pubkey for forrestii.torproject.org transfer"  # noqa: E501

    stream = io.BytesIO(header)
    ensure_ssh_key_stream(stream, key, key_comment)
    stream.seek(0)
    assert stream.read() == header + key_comment + b"\n" + key + b"\n"
    ensure_ssh_key_stream(stream, key, key_comment)
    stream.seek(0)
    assert stream.read() == header + key_comment + b"\n" + key + b"\n"


@task
def backup_file(con, path):
    _backup_file(con, path)


def _backup_file(con, path):
    backup_path = path + ".bak"
    logging.info("copying %s to %s on %s", path, backup_path, con.host)
    if not con.config.run.dry:
        res = con.run("cp %s %s" % (path, backup_path), warn=True)
        if res.failed:
            logging.warning("failed backup file %s, assuming backup is current", path)
    return backup_path


@task
def diff_file(con, left_path, right_path):
    return _diff_file(con, left_path, right_path)


def _diff_file(con, left_path, right_path):
    # TODO: fails if file is missing, should default to /dev/null
    # fixing this requires another round-trip, so fuck it.
    return con.run("diff -u %s %s" % (left_path, right_path), warn=True)


@task
def rewrite_file(con, path, content):
    _rewrite_file(con, path, content)


def _rewrite_file(con, path, content):
    """write a new file, keeping a backup

    This overwrites the given PATH with CONTENT, keeping a backup in a
    .bak file and showing a diff.
    """
    backup_path = _backup_file(con, path)
    logging.info("writing file %d bytes in %s on %s", len(content), path, con.host)
    _write_to_file(con, path, content)
    return _diff_file(con, backup_path, path)


@task
def rewrite_interfaces(
    con,
    ipv4_address,
    ipv4_subnet,
    ipv4_gateway=None,
    ipv6_address=None,
    ipv6_subnet=None,
    ipv6_gateway=None,
    path="/etc/network/interfaces",
):
    """rewrite an /etc/network/interfaces file

    This writes the given IPv4 and IPv6 addresses to the given
    interfaces(5) file, keeping a backup in interfaces.bak (it reuses
    the rewrite-file task).

    It will also include a header to source the interfaces.d directory
    and a loopback interfaces.

    It hardcodes the `eth0` interface.
    """
    # this is basically a stub for the rewrite_interfaces_ifconfig
    # file so it can be called as a fabric task.
    #
    # TODO: do SLAAC based on ipv6_net?
    ipconf = ifconfig(
        ipv4_address, ipv4_subnet, ipv4_gateway, ipv6_address, ipv6_subnet, ipv6_gateway
    )
    return rewrite_interfaces_ifconfig(con, ipconf, path)


def rewrite_interfaces_ifconfig(con, ipconf, path="/etc/network/interfaces"):
    """write an /etc/network/interfaces file

    This writes the given ifconfig namedtuple into the given
    interfaces(5) file, keeping a backup in .bak (it reuses the
    rewrite-file task).

    It will also include a header to source the interfaces.d directory
    and a loopback interfaces.

    It hardcodes the `eth0` interface.
    """
    content = """# This file describes the network interfaces available on your system
# and how to activate them. For more information, see interfaces(5).

source /etc/network/interfaces.d/*

# The loopback network interface
auto lo
iface lo inet loopback

# The primary network interface
auto eth0
"""

    if ipconf.ipv6:
        content += f"""
# IPv6 configuration
iface eth0 inet6 static
    accept_ra 0
    address {ipconf.ipv6}/{ipconf.ipv6_subnet}
"""
        if ipconf.ipv6_gateway:
            content += f"    gateway {ipconf.ipv6_gateway}\n"

    content += f"""
# IPv4 configuration
# after IPv6 to avoid race conditions in accept_ra
iface eth0 inet static
    address {ipconf.ipv4}/{ipconf.ipv4_subnet}
"""
    if ipconf.ipv4_gateway:
        content += f"    gateway {ipconf.ipv4_gateway}\n"
    logging.debug("generated %s: %s", path, content)
    return _rewrite_file(con, path, content)


@task
def create_swapfile(con, path="/swapfile", size="1GiB", fstab="/etc/fstab"):
    """creates a swap file and writes an fstab entry"""
    try:
        from humanfriendly import parse_size, InvalidSize
    except ImportError:
        logging.warning(
            "humanfriendly library unavailable, parsing size %s as an integer", size
        )
        parse_size = int
    try:
        size_bytes = parse_size(size)
    except (ValueError, InvalidSize) as e:
        logging.error("failed to parse size %s: %s", size, e)
    size_blocks = round(size_bytes / 4096)  # 4k blocks
    if con.config.run.dry:
        return
    logging.info("creating %s swap file on %s", size, path)
    try:
        con.run("dd if=/dev/zero of=%s bs=4KB count=%s" % (path, size_blocks))
        con.run("chmod 0 %s" % path)
        con.run("mkswap %s" % path)
    except invoke.exceptions.UnexpectedExit as e:
        logging.error("error creating swapfile in %s: %s", path, e)
        return
    logging.info("writing out swap entry to %s", fstab)
    write_to_file(con, fstab, "/swapfile none swap sw 0 0".encode("ascii"), mode="ab")


def parse_rewrite_interfaces_diff(output):
    """extracts old IP addresses from the diff

    XXX: we should maybe check LDAP instead? this is just too hackish
    """
    regex = re.compile(
        r"^-\s+address\s+(?:(?P<ipv4_address>\d+\.\d+\.\d+\.\d+)|(?P<ipv6_address>[\da-f]+:[\da-f:]+))/\d+\s*$",
        re.MULTILINE,
    )
    # placeholder values in case we don't find anything
    ipv4_address_old, ipv6_address_old = None, None
    for match in regex.finditer(output):
        if match.group("ipv4_address"):
            ipv4_address_old = match.group("ipv4_address")
        if match.group("ipv6_address"):
            ipv6_address_old = match.group("ipv6_address")
    return ipv4_address_old, ipv6_address_old


@task
def rewrite_hosts(con, fqdn, ipv4_address, ipv6_address=None, path="/etc/hosts"):
    _rewrite_hosts(con, fqdn, ipv4_address, ipv6_address, path)


def _rewrite_hosts(con, fqdn, ipv4_address, ipv6_address=None, path="/etc/hosts"):
    backup_path = _backup_file(con, path)
    if con.config.run.dry:
        logging.info("skipping hosts file rewriting in dry run")
        return
    logging.info("rewriting host file %s on %s", path, con.host)
    with con.sftp().file(path, mode="ab+") as fp:
        rewrite_hosts_file(
            fp,
            fqdn.encode("ascii"),
            ipv4_address.encode("ascii"),
            ipv6_address.encode("ascii") if ipv6_address else None,
        )
    _diff_file(con, backup_path, path)


def rewrite_hosts_file(stream, fqdn, ipv4_address, ipv6_address=None):
    hostname, _ = fqdn.split(b".", 1)
    line = b"%s %s %s" % (ipv4_address, fqdn, hostname)
    logging.debug("ensuring %s in hosts file", line)
    ensure_line_stream(stream, line, match=rb"^[.\d]+\s+" + re.escape(fqdn) + rb"\b.*$")
    if ipv6_address is not None:
        line = b"%s %s %s" % (ipv6_address, fqdn, hostname)
        logging.debug("ensuring %s in hosts file", line)
        ensure_line_stream(
            stream, line, match=rb"^[:0-9a-fA-F]+\s+" + re.escape(fqdn) + rb"\b.*$"
        )


def test_rewrite_hosts_file():
    import io

    stream = io.BytesIO()
    i = b"1.2.3.4"
    rewrite_hosts_file(stream, b"test.example.com", i)
    stream.seek(0)
    assert stream.read() == b"1.2.3.4 test.example.com test\n"
    rewrite_hosts_file(stream, b"test.example.com", i)
    stream.seek(0)
    assert stream.read() == b"1.2.3.4 test.example.com test\n"
    i = b"1.2.3.5"
    rewrite_hosts_file(stream, b"test.example.com", i)
    stream.seek(0)
    assert stream.read() == b"1.2.3.5 test.example.com test\n"

    stream = io.BytesIO(
        b"# this is a hosts file\n138.201.212.234        forrestii.torproject.org forrestii\n"
    )  # noqa: E501
    rewrite_hosts_file(stream, b"forrestii.torproject.org", i)
    stream.seek(0)
    assert (
        stream.read()
        == b"# this is a hosts file\n1.2.3.5 forrestii.torproject.org forrestii\n"
    )  # noqa: E501

    stream = io.BytesIO(b"138.201.212.234\tforrestii.torproject.org\tforrestii\n")
    rewrite_hosts_file(stream, b"forrestii.torproject.org", i)
    stream.seek(0)
    assert stream.read() == b"1.2.3.5 forrestii.torproject.org forrestii\n"

    stream = io.BytesIO(b"::1\tforrestii.torproject.org\tforrestii\n")
    rewrite_hosts_file(stream, b"forrestii.torproject.org", i)
    stream.seek(0)
    assert (
        stream.read()
        == b"::1\tforrestii.torproject.org\tforrestii\n1.2.3.5 forrestii.torproject.org forrestii\n"
    )  # noqa: E501

    stream = io.BytesIO(b"::1\tforrestii.torproject.org\tforrestii\n")
    rewrite_hosts_file(stream, b"forrestii.torproject.org", i, b"fe80::")
    stream.seek(0)
    assert (
        stream.read()
        == b"fe80:: forrestii.torproject.org forrestii\n1.2.3.5 forrestii.torproject.org forrestii\n"
    )  # noqa: E501


@task
def password_change(
    con, user="root", pass_dir="root", chroot=None, pass_save=True, show_pass=False
):
    """change a user password on a host, storing the password in pass"""
    try:
        hostname = con.host
    except AttributeError:
        hostname = socket.getfqdn()
    if user != "root":
        hostname += f"-{user}"

    # default length here is "32 bytes" which sums up to about 43
    # characters, which is fine by me
    passwd = secrets.token_urlsafe()
    # but make sure it's long enogh anyway
    assert len(passwd) > 40
    if pass_save:
        pass_insert_local(f"{pass_dir}/{hostname}", passwd)

    stream = io.StringIO(f"{user}:{passwd}")
    if show_pass:
        logging.info("changing %s password on %s to %s", user, hostname, passwd)
    else:
        logging.info("changing %s password on %s", user, hostname)
    if chroot:
        con.run(f"sudo chpasswd -R {chroot}", in_stream=stream)
    else:
        con.run("chpasswd", in_stream=stream)


def pass_insert_local(name, passwd):
    """insert the given password inside the pass database

    We originally tried to use pass generate but that outputs control
    characters to fucking show the password in *colors*. So fuck that.
    """
    logging.info("saving %d bytes password as %s", len(passwd), name)
    subprocess.run(
        ["pass", "insert", "--multiline", name],
        check=True,
        input=passwd,
        encoding="utf-8",
    )


@task
def mount(con, device, path, options="", warn=None):
    """mount a device"""
    command = "mount %s %s %s" % (device, path, options)
    # XXX: error handling?
    return con.run(command, warn=warn)


@task
def umount(con, path):
    """umount a device"""
    # XXX: error handling?
    return con.run("umount %s" % path)


@contextmanager
def mount_then_umount(con, device, path, options="", warn=None):
    """convenient context manager for mount/umount"""
    try:
        yield mount(con, device, path, options, warn)
    finally:
        umount(con, path)


ifconfig = namedtuple(
    "ifconfig", "ipv4 ipv4_subnet ipv4_gateway ipv6 ipv6_subnet ipv6_gateway"
)  # noqa: E501


@task
def ipv6_slaac(con, ipv6_subnet, mac, hide=True, dry=False):
    """compute IPv6 SLAAC address from subnet and MAC address

    This uses the ipv6calc command.

    .. TODO:: rewrite in python-only?
    """
    command = [
        "ipv6calc",
        "--action",
        "prefixmac2ipv6",
        "--in",
        "prefix+mac",
        "--out",
        "ipv6",
        ipv6_subnet,
        mac,
    ]
    logging.debug("manual SLAAC allocation with: %s", " ".join(command))
    try:
        # XXX: error handling?
        return con.run(" ".join(command), hide=hide, dry=dry).stdout.strip()
    except invoke.exceptions.UnexpectedExit as e:
        logging.error("cannot find IPv6 address, install ipv6calc: %s", e)


def test_ipv6_slaac():
    con = invoke.Context()
    mac = "00:66:37:f1:bb:6b"
    network = "2a01:4f8:fff0:4f::"
    expected = "2a01:4f8:fff0:4f:266:37ff:fef1:bb6b"
    assert expected == ipv6_slaac(con, network, mac)


def find_context(hostname, config=None) -> Connection | invoke.Context:
    if isinstance(hostname, (Connection, invoke.Context)):
        return hostname
    else:
        return Connection(hostname, config=config)


@task
def whereis(instance):
    """find on which metal the given virtual machine is hosted"""
    ldap_con = LdapContext().bind()
    filter = "(hostname=%s)" % instance.host
    for dn, attrs in ldap_con.search_hosts(filterstr=filter):
        logging.debug("dn: %s, attrs: %r" % (dn, attrs))
        print(
            "host %s is " % attrs.get("hostname")[0].decode("utf-8"), flush=True, end=""
        )
        parent = attrs.get("physicalHost")
        if parent is not None:
            print("on " + parent[0].decode("utf-8"))
            continue
        # no physical host, show location
        parent = attrs.get("l")
        if parent is None:
            # no location, say so
            print("in an [unknown location]")
            continue
        else:
            print("a physical machine located in " + parent[0].decode("utf-8"))
            continue


class AptParsingError(Exception):
    pass


@dataclass(frozen=True)
class AptPackageAvailability:
    pkgname: str
    installed: str
    candidate: str

    def __str__(self):
        return f"{self.pkgname} ({self.installed}, {self.candidate})"


@task(iterable=["reboot_services"])
def needs_reboot(
    con: Connection | invoke.Context,
    reboot_services: Iterable[str] = ("ganeti.service",),
    all_services: bool = False,
) -> list[str]:
    needrestart = con.run(
        "needrestart -b",
        warn=True,
    )
    # Params to .run() that can return None are not set
    assert needrestart is not None
    state = parse_needrestart(needrestart.stdout, reboot_services, all_services)
    if state:
        logging.warning("reboot required on host %s: %s", con.host, state)
    else:
        logging.info("no reboot required on host %s", con.host)
    return state


NEEDRESTART_RE = re.compile(
    r"^NEEDRESTART-(?P<key>[^:]+):\s*(?P<val>.*)$", re.MULTILINE
)


def parse_needrestart(
    output: str,
    reboot_services: Iterable[str] = (),
    all_services: bool = False,
) -> list[str]:
    """This parses the given needrestart output and returns the services
    that we believe will require a reboot.

    This specifically encodes business logic like "apache2 doesn't
    need a reboot but the kernel, microcode updates, and Ganeti do".

    We explicitely grep for ganeti, even though that's actually wrong:
    we shouldn't necessarily reboot on Ganeti upgrades, only if the
    qemu* processes need a reboot. But needrestart doesn't currently
    distinguish on those, so we're going to be more ... "liberal" (?)
    and reboot anyways.

    >>> output = "\\n".join([
    ...   'NEEDRESTART-KCUR: 5.10.0-10-amd64',
    ...   'NEEDRESTART-KEXP: 5.10.0-11-amd64',
    ...   'NEEDRESTART-KSTA: 3',
    ...   'NEEDRESTART-UCSTA: 2',
    ...   'NEEDRESTART-UCCUR: 0x071a',
    ...   'NEEDRESTART-UCEXP: 0x071b',
    ...   'NEEDRESTART-SVC: ganeti.service',
    ...   'NEEDRESTART-SVC: apache2.service',
    ... ]) + "\\n"
    >>> services = parse_needrestart(output, ('ganeti.service', ), False)
    >>> 'kernel' in services
    True
    >>> 'microcode' in services
    True
    >>> 'ganeti.service' in services
    True
    >>> 'apache2.service' in services
    False
    >>> services = parse_needrestart(output, all_services=True)
    >>> 'apache2.service' in services
    True
    """
    # grep for:
    # "NEEDRESTART-KSTA: 1" (no restart, or should we ignore unknown?)
    # "NEEDRESTART-UCSTA: 1" (no restart)
    # "NEEDRESTART-SVC: systemd.service" (for systemd.service?, dbus-daemon, qemu-system-x86: restart)
    # dbus.service
    # ganeti.service (but just the qemu processes?)
    #
    # systemd.service doesn't exist, it's init.scope, not sure how
    # needrestart sees those
    #
    # see also
    # https://github.com/liske/needrestart/issues/230
    # https://github.com/xneelo/hetzner-needrestart/issues/23

    # list of things that make us want a reboot
    needs_reboot: list[str] = []
    # map some weird needrestart keys to a human-readable string
    needrestart_key_map = {
        "UCSTA": "microcode",
        "KSTA": "kernel",
    }
    needrestart_facts = {}
    for m in NEEDRESTART_RE.finditer(output):
        logging.debug("key/val: %s/%s", m.group("key"), m.group("val"))
        needrestart_facts[m.group("key")] = m.group("val")
        if m.group("key") in needrestart_key_map.keys():
            state = int(m.group("val").strip())
            if state != 1:
                needs_reboot_object = needrestart_key_map.get(m.group("key"))
                # We've checked this just above, this assert should not trigger
                # but it affirms to the static type generator that we've
                # properly rejected the case where .get() returned a None
                assert needs_reboot_object is not None
                needs_reboot.append(needs_reboot_object)
            else:
                logging.debug(
                    "%s state %d does not require a reboot",
                    needrestart_key_map.get(m.group("key")),
                    state,
                )
        elif m.group("key") == "SVC":
            if all_services or m.group("val").strip() in reboot_services:
                needs_reboot.append(m.group("val"))
            else:
                logging.debug(
                    "ignoring service %s, does not require a reboot", m.group("val")
                )
    assert needrestart_facts
    logging.debug("facts: %r", needrestart_facts)
    if needrestart_facts.get("KCUR"):
        logging.debug(
            "current kernel: %s, expected: %s",
            needrestart_facts.get("KCUR"),
            needrestart_facts.get("KEXP"),
        )
    if needrestart_facts.get("UCCUR"):
        logging.debug(
            "current microcode: %s, expected: %s",
            needrestart_facts.get("UCCUR"),
            needrestart_facts.get("UCEXP"),
        )
    return needs_reboot


@task
def pending_upgrades(con: Connection):
    """list pending upgrades on given host"""
    logging.info("loading package list from %s", con.host)
    con.run("apt-get update", warn=True, hide=True)
    result = con.run("apt-cache policy ~i", warn=True, hide=True)
    try:
        _handle_pending_upgrades(con, result)
    except AptParsingError as e:
        raise invoke.exceptions.Exit(str(e))


def _handle_pending_upgrades(
    con: Connection, result: Result
) -> set[AptPackageAvailability]:
    if result.failed:
        raise AptParsingError("apt-cache policy ~i failed?")
    if result.stderr.startswith("N: Unable to locate package"):
        raise AptParsingError(
            "unable to extract package list: apt-cache policy ~i unsupported"
        )

    counter = 0
    outofdate = set()
    # ideally, we'd stream that data out of the result object, but i
    # couldn't figure out teh out_stream argument thing...
    for pkg in _parse_apt_cache_policy_stream(io.StringIO(result.stdout)):
        counter += 1
        if pkg.installed != pkg.candidate:
            outofdate.add(pkg)
    if not counter:
        raise AptParsingError(
            "no packages found, assuming apt-cache policy ~i unsupported"
        )
    if len(outofdate):
        logging.warning(
            "found %d pending upgrades out of %d packages on host %s: %s",
            len(outofdate),
            counter,
            con.host,
            " ".join(sorted(map(str, outofdate))),
        )
    else:
        logging.info("all %d packages up to date on %s", counter, con.host)
    return outofdate


def _parse_apt_cache_policy_stream(stream: IO) -> Iterator[AptPackageAvailability]:
    current_package = None
    current_installed = None
    current_candidate = None
    for line in stream.readlines():
        if line.startswith("N: Unable to locate package"):
            raise AptParsingError(
                "unable to extract package list: apt-cache policy ~i unsupported"
            )
        if line.startswith(" "):
            if ":" not in line:
                continue
            key, value = line.strip().split(":", maxsplit=1)
            if key == "Installed":
                current_installed = value.strip()
            elif key == "Candidate":
                current_candidate = value.strip()
        else:
            if current_package:
                yield AptPackageAvailability(
                    current_package, current_installed or "", current_candidate or ""
                )
            current_package = line.strip().strip(":")
    if current_package:
        if not current_installed or not current_candidate:
            raise AptParsingError(
                "failed to parse apt-cache policy output, missing installed or candidate field on last entry"
            )
        yield AptPackageAvailability(
            current_package, current_installed, current_candidate
        )
