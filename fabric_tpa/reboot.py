#!/usr/bin/python3
# coding: utf-8

""""""

# Copyright (C) 2016 Antoine Beaupré <anarcat@debian.org>
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import division, absolute_import
from __future__ import print_function, unicode_literals

from enum import StrEnum
from contextlib import closing
from dataclasses import dataclass, field
from datetime import datetime, timedelta, timezone
import logging
import socket
import sys
import time

try:
    from fabric import task
except ImportError:
    sys.stderr.write(
        "cannot find fabric, install with `apt install python3-fabric`"
    )  # noqa: E501
    raise
from fabric.tasks import Connection

# no check required, fabric depends on invoke
import invoke
from invoke import Responder
from invoke.exceptions import ResponseNotAccepted, Failure, Exit
import paramiko.ssh_exception


from . import ganeti
from . import host
from . import local
from . import mandos
from . import silence
from .ui import input_bell, yes_no
from .kgb import relay_message as kgb_relay_message


DEFAULT_DELAY_DOWN_SECONDS = 30  # in seconds
DEFAULT_DELAY_DOWN = DEFAULT_DELAY_DOWN_SECONDS  # deprecated
DEFAULT_DELAY_UP_SECONDS = 300
DEFAULT_DELAY_UP = DEFAULT_DELAY_UP_SECONDS  # deprecated
DEFAULT_DELAY_HOSTS_SECONDS = 120
DEFAULT_DELAY_HOSTS = DEFAULT_DELAY_HOSTS_SECONDS  # deprecated
DEFAULT_DELAY_SHUTDOWN_MINUTES = 10
DEFAULT_DELAY_SHUTDOWN = DEFAULT_DELAY_SHUTDOWN_MINUTES
DEFAULT_REASON = "rebooting for security upgrades"


def wait_for_shutdown(
    con: Connection, wait_timeout: int = DEFAULT_DELAY_DOWN, wait_confirm: int = 3
) -> bool:
    """wait for host to shutdown

    This pings the host and waits one second until timeout is expired
    or until it stops pinging.

    Returns True if the box stops pinging before timeout, or False if
    the box still pings after the timeout expired.
    """
    confirmations = 0
    for _ in local.trange(wait_timeout):
        if tcp_ping_host(con):
            # port is open, so we didn't timeout, sleep the required delay
            # TODO: discount the ping time to get a real one second delay?
            time.sleep(1)
        else:
            if confirmations >= wait_confirm:
                break
            else:
                confirmations += 1
    return confirmations >= wait_confirm


def wait_for_ping(con: Connection, timeout: int = DEFAULT_DELAY_UP) -> bool:
    """wait for host to ping

    This tries to ping the host until it responds or until the timeout
    expires.

    This returns true if the host pings or False if the timeout
    expires and the host still does not ping.
    """
    for _ in local.trange(timeout):
        # this will "sleep" one second if host is unreachable
        if tcp_ping_host(con):
            return True
    return tcp_ping_host(con)


def wait_for_live(con: Connection, delay_up: int = DEFAULT_DELAY_UP) -> bool:
    if not wait_for_ping(con, delay_up):
        logging.warning(
            "host %s did not return after %d seconds, aborting", con.host, delay_up
        )
        return False

    logging.info("host %s seems back online, checking uptime", con.host)
    # TODO: this fails on new hosts waiting for the LUKS password:
    # paramiko.ssh_exception.BadHostKeyException: Host key for server '88.99.194.57' does not match: got 'AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBMmnz01y767yiws7ZjBnFtWtR7GWv4u5R1fBXKERaarVx38lUUbyA0nuufNwhX3/KX6fcuuoBZQqFDamB3XwKD8=', expected 'AAAAE2VjZHNhLXNoYTItbmlzdHAyNTYAAAAIbmlzdHAyNTYAAABBBOu/GXkUtqJ9usIINpWyJnpnul/+vvOut+JKvLnwdbrJn/0hsD1S4YhmHoxIwMbfD8jzYghFfKvXSZvVPgH3lXY='  # noqa: E501

    # this is a Fabric "watcher" that will fail if we get a LUKS prompt
    # TODO: actually allow the user to provide a LUKS passphrase?
    responder = SentinelResponder(
        sentinel=r"Please unlock disk .*:",
    )
    # see if we can issue a simple command
    for _ in local.trange(3):
        try:
            res = con.run("uptime", watchers=[responder], pty=True, warn=True)
            # Params to .run() that can return None are not set
            assert res is not None
        # got the "unlock" prompt instead of a shell
        # XXX: why don't we get our exception from the watcher?
        # instead we need to catch invoke's Failure here
        except (ResponseNotAccepted, Failure):
            logging.warning(
                "server waiting for crypto password, sleeping %d seconds for mandos",
                DEFAULT_DELAY_DOWN,
            )
            wait_for_shutdown(con, wait_confirm=1)
        except paramiko.ssh_exception.SSHException as e:
            logging.warning(
                "cannot find a valid SSH key anymore (%s), is your cryptographic token plugged in?",
                e,
            )
            input_bell("pay attention to your cryptographic token and press enter: ")
        # failed to connect to the host
        except (OSError, EOFError) as e:
            logging.error(
                "host %s cannot be reached by fabric, sleeping: %r", con.host, e
            )
        else:
            # command was issued, but failed
            if res.failed:
                logging.error("uptime command failed on host %s: %s", con.host, res)
                return False
            # command suceeded
            else:
                return True
        # if we got here, we're in the "not reachable" state
        wait_for_ping(con)
    return False


class ShutdownType(StrEnum):
    """the different flags that can be passed to the shutdown command

    This is not called "Flag" because that has a specific meaning for
    Enum classes, specifically stuff that can be combined with bitwise
    operators.
    """

    reboot = "-r"
    halt = "-h"
    wall = "-k"
    cancel = "-c"

    @classmethod
    def _missing_(cls, value):
        """support using the symbolic names instead of just flags

        This hooks somewhat deep in the Enum class, but it's the
        fallback when an argument (the `value` above) is not found in
        the Enum's "values" (-r, -h, etc). In our case, we check the
        symbolic names (the "members": "reboot", "halt", etc) as well.
        """
        if value in cls.__members__:
            return cls[value]
        return super()._missing(cls, value)

    def __str__(self) -> str:
        """return the actual string representation

        the default string representation of an Enum is Class.field,
        not the actual value's representation. so instead of returning
        (say) 'ShutdownType.reboot', we return '-r' here.
        """
        return self.value


@task
def shutdown(
    con: Connection,
    kind: str = ShutdownType.reboot,
    reason: str = DEFAULT_REASON,
    delay: int = DEFAULT_DELAY_SHUTDOWN,
    notify: bool = True,
):
    """trigger a shutdown or reboot on the host"""
    # TODO: notify prometheus, see https://gitlab.torproject.org/tpo/tpa/team/-/issues/41811
    if kind == ShutdownType.reboot:
        if delay > 0:
            msg = f"scheduled reboot on host {con.host} in {delay} minutes for {reason}"
        else:
            msg = f"rebooting host {con.host} for {reason}"
    elif kind == ShutdownType.halt:
        if delay > 0:
            msg = (
                f"scheduled shutdown on host {con.host} in {delay} minutes for {reason}"
            )
        else:
            msg = f"stopping host {con.host} for {reason}"
    elif kind == ShutdownType.cancel:
        msg = f"canceling shutdown on host {con.host} for {reason}"
    else:
        logging.warning("unusual reboot type used: %s, passing blindly", kind)
        msg = None

    if notify and msg and reason != "failsafe":
        kgb_relay_message(msg)
    cmd = "shutdown %s" % kind
    if kind in (ShutdownType.halt, ShutdownType.reboot):
        cmd += ' +%d "%s"' % (delay, reason)
    msg = "running %s on %s" % (cmd, con.host)
    logging.info(msg)
    # XXX: error handling?
    return con.run(cmd)


# XXX: this doesn't actually work, figure out why. we use the long
# list everywhere instead now. see also:
# https://github.com/fabric/fabric/issues/2061
class FabricException(EOFError, OSError, paramiko.ssh_exception.SSHException):
    pass


def parse_apt_list(data):
    for line in data.split("\n"):
        if line == "Listing..." or not line.strip():
            continue
        try:
            package, _ = line.split("/", maxsplit=1)
        except ValueError:
            logging.warning("could not parse apt-list output, skipping line: %s", line)
            continue
        yield package


@dataclass
class ShutdownHooks:
    """a collection of pre/post hooks to be ran before and after a shutdown is ran on a host

    This used to be a grab-bag mix of code and functions in and out of
    shutdown_and_wait, constantly moving the same data structures
    around, sometimes badly.

    Having all this wrapped up in a class makes this easier to think
    about and manage.
    """

    con: Connection
    master_con: Connection = None
    shutdown_instances: list[str] = field(default_factory=list)
    migrated_instances: list[str] = field(default_factory=list)

    def pre(self) -> None:
        """hooks that run right before the actual shutdown() of the main host

        Currently runs upgrades to profit from the occasion."""
        self._pre_run_pending_upgrades()
        if mandos.disabled(self.con):
            if yes_no("host %s disabled in Mandos, enable?" % self.con.host):
                mandos.enable(self.con)

    def post_wait(self) -> None:
        """hooks that run after we've succesfully waited for the host to return

        Currently empty.

        Doesn't run on halt or if the waiting failed."""
        pass

    def _pre_run_pending_upgrades(self) -> None:
        """look for pending upgrades and do upgrade everything before rebooting

        This will prompt the user and wait for confirmation if there
        was an uprade done. If no upgrades are pending, it just
        continues.
        """
        con = self.con
        logging.info("looking for pending upgrades...")
        ret = con.run("apt list --upgradable", warn=True)
        # Params to .run() that can return None are not set
        assert ret is not None
        if ret.failed:
            logging.warning(
                "failed to list packages on remote host, assuming no pending upgrade"
            )
            return
        packages = list(parse_apt_list(ret.stdout))
        if not len(packages):
            logging.info("no pending package upgrades")
            return

        logging.info(
            "scheduling a failsafe reboot for %d minutes from now, in case upgrade destroys the network",
            DEFAULT_DELAY_SHUTDOWN,
        )
        try:
            shutdown(con, ShutdownType.reboot, "failsafe", DEFAULT_DELAY_SHUTDOWN)
        except invoke.UnexpectedExit as e:
            raise Exit("unexpected error issuing reboot on %s: %s" % (con.host, e))
        except (EOFError, OSError, paramiko.ssh_exception.SSHException) as e:
            logging.warning(
                "failed to connect to %s, confused but trying anyway: %s", con.host, e
            )

        ret = con.run("apt-get upgrade -y", warn=True)
        # Params to .run() that can return None are not set
        assert ret is not None
        if ret.failed:
            logging.warning("apt upgrade failed, ignoring error")

        input_bell(
            "confirm packages upgraded correctly before performing reboots, press enter to continue: "
        )
        # logging.info("upgrading openvswitch, networking might be lost")
        # con.run("apt-get install openvswitch-common openvswitch-security")

    def pre_ganeti_checks(
        self,
        ganeti_migrate: bool,
        delay_shutdown: int,
        delay_down: int,
        delay_up: int,
        silence_ends_at: datetime | str | None = None,
    ) -> bool:
        """check for a Ganeti node. if present, migrate or shutdown instances off of it.

        This is a separate function to clarify the code and reduce
        coupling with the main shutdown_and_wait function, mainly so
        that we don't have to keep track of delay_* members at the
        class level or in pre().

        But if we *do* need those delays in another hook, that should
        be merged in the main pre() function.
        """
        try:
            master = ganeti.getmaster(self.con)
        except (OSError, paramiko.ssh_exception.SSHException, EOFError) as e:
            raise Exit(
                "failed to contact host %s, aborting reboot: %s" % (self.con.host, e)
            )
        except invoke.exceptions.Failure:
            logging.info("host %s is not a ganeti node", self.con.host)
            return False
        self.master_con = host.find_context(master, config=self.con.config)

        if ganeti_migrate:
            (
                self.migrated_instances,
                self.shutdown_instances,
            ) = ganeti.migrate_all_instances(
                self.con,
                self.master_con,
                delay_shutdown,
                delay_down,
                delay_up,
                silence_ends_at,
            )
        else:
            # raise Exit("failed to shutdown all instances on node %s, aborting" % con.host)
            self.shutdown_instances = ganeti.stop_all_instances(
                self.con,
                self.master_con,
                delay_shutdown,
                delay_down,
                delay_up,
                silence_ends_at,
            )
        return True

    def post_ganeti_checks(self, migrate_back: bool) -> None:
        """migrate back or start instances after ganeti reboot

        This is not called as part of the normal post_wait() hook for
        the same reason as the pre() is separate.
        """
        if self.shutdown_instances:
            assert self.master_con, "no master_con instance???"
            kgb_relay_message(
                "starting %s hosts on %s: %s"
                % (
                    len(self.shutdown_instances),
                    self.con.host,
                    " ".join(self.shutdown_instances),
                )
            )
            self.master_con.run(
                "gnt-instance start --force-multiple %s"
                % " ".join(self.shutdown_instances)
            )
        if migrate_back and self.migrated_instances:
            assert self.master_con, "no master_con instance???"
            kgb_relay_message(
                "migrating %d instances back to %s: %s"
                % (
                    len(self.migrated_instances),
                    self.con.host,
                    " ".join(self.migrated_instances),
                )
            )
            ganeti.migrate_instances(self.master_con, self.migrated_instances)


@task
def shutdown_and_wait(
    con: Connection | invoke.Context,
    kind: str = ShutdownType.reboot,
    reason: str = DEFAULT_REASON,
    delay_shutdown: int = DEFAULT_DELAY_SHUTDOWN,
    delay_down: int = DEFAULT_DELAY_DOWN,
    delay_up: int = DEFAULT_DELAY_UP,
    silence_ends_at: datetime | str | None = None,
    ganeti_checks: bool = True,
    ganeti_migrate: bool = True,
    ganeti_migrate_back: bool = True,
) -> bool:
    """shutdown the machine and possibly wait for the box to return"""
    # TODO: support pooling multiple connexions here.
    # Could this be done with a ThreadingGroup?
    #
    # we made a loop for `ShutdownType.halt` shutdowns in
    # ganeti.stop_instances, maybe it could be reused here...

    # we don't handle wall or cancel kinds here, too complicated and
    # they don't need to wait
    assert kind not in (ShutdownType.wall, ShutdownType.cancel)

    now = datetime.now(timezone.utc)
    then = now + timedelta(minutes=delay_shutdown)
    # The UnexpectedReboot alert will catch any reboot that happened in the last
    # hour so we always want to silence for at least 1h.
    if silence_ends_at is None:
        silence_ends_at = (
            then
            + timedelta(hours=1)
            + timedelta(seconds=delay_down)
            + timedelta(seconds=delay_up)
        )

    hooks = ShutdownHooks(con)
    hooks.pre()
    if ganeti_checks:
        if hooks.pre_ganeti_checks(
            ganeti_migrate, delay_shutdown, delay_down, delay_up, silence_ends_at
        ):
            # shorter delay, as the node will be empty
            delay_shutdown = 0
    else:
        logging.warning("not checking if %s is a Ganeti node, as requested", con.host)

    try:
        shutdown(con, kind, reason, delay_shutdown)
    except invoke.UnexpectedExit as e:
        raise Exit("unexpected error issuing reboot on %s: %s" % (con.host, e))
    except (EOFError, OSError, paramiko.ssh_exception.SSHException) as e:
        logging.warning("failed to connect to %s, assuming down: %s", con.host, e)

    silence.create(
        invoke.Context(),
        [f"alias={con.host}"],
        "silencing all alerts for %s for reboot" % con.host,
        starts_at=then,
        ends_at=silence_ends_at,
    )
    # XXX: use a state machine to follow where we are?
    if delay_shutdown > 0:
        logging.info(
            "waiting %s minutes for reboot to complete at %s (now is %s)",
            delay_shutdown,
            then.isoformat(timespec="seconds"),
            now.isoformat(timespec="seconds"),
        )
        # NOTE: we convert minutes to seconds here
        local.sleep_with_progress(delay_shutdown * 60)

    now = datetime.now(timezone.utc)
    then = now + timedelta(seconds=delay_down)
    logging.info(
        "host shutting down, waiting up to %d seconds for confirmation, at %s (now is %s)",  # noqa: E501
        delay_down,
        then.isoformat(timespec="seconds"),
        now.isoformat(timespec="seconds"),
    )
    if not wait_for_shutdown(con, delay_down):
        raise Exit(
            "host %s was still up after %d seconds, aborting%s"
            % (
                con.host,
                delay_down,
                (
                    ", instances may need to be migrated back: %s"
                    % hooks.migrated_instances
                    if hooks.migrated_instances
                    else ""
                ),
            )
        )
    if kind == ShutdownType.halt:
        logging.info("host %s shutdown", con.host)
        return True

    now = datetime.now(timezone.utc)
    then = now + timedelta(seconds=delay_up)
    logging.info(
        "host down, waiting %d seconds for host to go up, at %s (now is %s)",
        delay_up,
        then,
        now,
    )
    if not wait_for_live(con, delay_up=delay_up):
        raise Exit(
            "could not check uptime on %s, assuming reboot failed%s"
            % (
                con.host,
                (
                    ", instances may need to be migrated back: %s"
                    % hooks.migrated_instances
                    if hooks.migrated_instances
                    else ""
                ),
            )
        )
    hooks.post_wait()
    if ganeti_checks:
        hooks.post_ganeti_checks(ganeti_migrate_back)
    return True


# XXX version 1.0 of invoke has a FailingResponder that seems to be
# implementing thie same functionality. Should we switch to using that instead?
class SentinelResponder(Responder):
    """Watcher which will fail only if the sentinel string is found, but
    will otherwise not respond.
    """

    def __init__(self, sentinel: str) -> None:
        self.sentinel = sentinel
        self.failure_index = 0

    # XXX note that the function signature here does not correspond to the one
    # in the Responder class: there, a generator is returned, not a list.
    # Adding a type annotation for the return value here triggers this error in
    # mypy.
    def submit(self, stream: str):
        if self.pattern_matches(stream, self.sentinel, "failure_index"):
            err = "Unexpected sentinal output: {!r}!".format(self.sentinel)
            raise ResponseNotAccepted(err)
        # do not write anything in response. the intuitive response
        # (`yield`) here will not do what we expect because it will
        # `yield None` and invoke will try to write `None` to the
        # stream, which is bad
        return []


@task
def reboot_and_wait(
    con: Connection,
    reason: str = DEFAULT_REASON,
    delay_shutdown: int = DEFAULT_DELAY_SHUTDOWN,
    delay_down: int = DEFAULT_DELAY_DOWN,
    delay_up: int = DEFAULT_DELAY_UP,
) -> bool:
    """reboot a machine and wait

    Convenience alias for shutdown_and_wait with a "reboot" kind."""
    return shutdown_and_wait(
        con,
        kind=ShutdownType.reboot,
        reason=reason,
        delay_shutdown=delay_shutdown,
        delay_down=delay_down,
        delay_up=delay_up,
    )


@task
def halt_and_wait(
    con: Connection,
    reason: str = DEFAULT_REASON,
    delay_shutdown: int = DEFAULT_DELAY_SHUTDOWN,
    delay_down: int = DEFAULT_DELAY_DOWN,
    delay_up: int = DEFAULT_DELAY_UP,
) -> bool:
    """halt a machine and wait

    Convenience alias for shutdown_and_wait with a "halt" kind."""
    return shutdown_and_wait(
        con,
        kind=ShutdownType.halt,
        reason=reason,
        delay_shutdown=delay_shutdown,
        delay_down=delay_down,
        delay_up=delay_up,
    )


def tcp_ping_host(con: Connection, port: int = 22, timeout: int = 1) -> bool:
    """ping the host by opening a TCP socket

    This is implemented using TCP because ICMP pings require raw
    sockets and so root access or ICMP capabilities. Besides, "ping"
    doesn't really tell us if a host has returned: what we want to
    know is if *services* are running and for that, TCP is a better
    model.

    The *port* argument determines which port is open (22 by default,
    since it's commonly available on all our hosts). The *timeout*
    argument determines how long we wait for a response (default: one
    second).

    TODO: maybe reuse https://github.com/networktocode/netutils
    https://netutils.readthedocs.io/en/latest/netutils/ping/index.html#netutils.ping.tcp_ping

    """

    try:
        with closing(socket.create_connection((con.host, port), timeout=timeout)):
            # do nothing with the socket, just test if it opens
            logging.debug("socket opened to %s:%d", con.host, port)
            return True
    except socket.timeout:
        logging.debug("timeout waiting for socket open to %s:%d", con.host, port)
        return False
    except (socket.herror, socket.gaierror) as e:
        logging.error("connect to %s:%d error: %s", con.host, port, e)
        return False
    except OSError as e:
        logging.debug("connect to %s:%d failed: %s, sleeping", con.host, port, e)
        time.sleep(1)
        return False
