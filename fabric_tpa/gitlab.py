"""GitLab tasks.

This module regroups tasks and code about operating GitLab in various
shapes or forms. It obsoletes the gitlab-tools repository and all code
from there should eventually be merged here.

It depends on the [python-gitlab] module, available in Debian as
`python3-gitlab`.

The `python-gitlab` module needs an INI-style configuration file that contains
the URL to one or more GitLab instances, as well as credentials.

The file is read from `~/.python-gitlab.cfg`, `/etc/python-gitlab.cfg` or can be
specified in the `PYTHON_GITLAB_CFG` environment variable or on the command-line
using the `--config-file` switch.

The configuration file looks like this:

    [global]
    default = torproject.org
    ssl_verify = true
    timeout = 10

    [torproject.org]
    url = https://gitlab.torproject.org
    private_token = <private token>
    api_version = 4

Details about the syntax and parameters of the configuration file can
be found in the [documentation]. You can obtain a `private_token`
from the [personal access tokens] page. Note that some (but not all)
scripts (e.g. `gitlab-tools`) also support getting the secret from the
`GITLAB_PRIVATE_TOKEN` environment variable.

[python-gitlab]: https://github.com/python-gitlab/python-gitlab
[documentation]: https://python-gitlab.readthedocs.io/en/stable/cli-usage.html#configuration-files
[personal access tokens]: https://gitlab.torproject.org/-/profile/personal_access_tokens
"""

from collections import defaultdict
import configparser
from dataclasses import dataclass, field
import logging
from typing import Iterator, List, Optional
import os
from os.path import exists, expanduser, join
import sys
import datetime
from datetime import timezone

try:
    from fabric import task, Connection
    from invoke import Context
    from invoke.exceptions import Exit
except ImportError:
    sys.stderr.write("cannot find fabric, install with `apt install python3-fabric`")
    raise

from fabric_tpa.ui import no_yes

from gitlab import Gitlab
from gitlab.v4.objects.projects import Project
from gitlab.v4.objects.users import User
import gitlab.config
import gitlab.exceptions

from .kgb import relay_message as kgb_relay_message


class GitLabConnector:
    """Wrapper around the Gitlab module to easily connect to a GitLab server

    This basically calls Gitlab.from_config() then auth(), setup
    debugging (if debug is True) and logging, alongside some more
    helpful extra logs for common exceptions.

    This, in particular, will add extra logging to
    gitlab.config.ConfigError and
    gitlab.exceptions.GitlabAuthenticationError which can probably
    just be caught and exited from by the caller without extra
    logging.
    """

    _singleton = None

    def __init__(
        self,
        gitlab_id: Optional[str] = None,
        config_files: Optional[List[str]] = None,
        debug: Optional[bool] = None,
    ) -> None:
        """This will load the specific configuration for the gitlab_id
        provided based on the .python-gitlab.cfg config file. It will
        authenticate with the server with auth() as well.

        If `debug` is True, it will also make sure enable_debug() is
        called.
        """
        # load configuration
        extra_config = {"private_token": os.environ.get("GITLAB_PRIVATE_TOKEN")}
        try:
            self.gitlab = Gitlab.merge_config(extra_config, gitlab_id, config_files)
        except gitlab.config.ConfigError as cfgerr:
            logging.warning(
                "Error loading configuration: %s, see "
                "https://python-gitlab.readthedocs.io/en/stable/cli-usage.html#configuration-files "
                "for configuration instructions",
                cfgerr,
            )
            raise cfgerr

        # enable gitlab request debugging
        if debug is None:
            debug = logging.getLogger("").level == logging.DEBUG
        if debug:
            self.gitlab.enable_debug()

        # authenticate to GitLab instance
        try:
            self.gitlab.auth()
        except gitlab.exceptions.GitlabAuthenticationError as e:
            config_file_path = config_files or "~/.python-gitlab.cfg"
            logging.error(
                "failed to login to GitLab: %s - maybe the token in %s expired?"
                % (e, config_file_path)
            )
            raise e
        logging.info("Successfully connected to %s", self.gitlab.url)

    @classmethod
    def singleton(cls, *args, **kwargs):
        """allow callers to reuse a single instance

        This allows callers to skip the `auth()` overhead that would
        otherwise be involved in creating multiple instances.
        """
        if cls._singleton is None:
            cls._singleton = cls(*args, **kwargs)
        return cls._singleton

    def group_projects(
        self,
        groups: list[str],
        archived: Optional[bool] = None,
        order_by: Optional[str] = None,
        reload_project: bool = True,
    ) -> Iterator[Project]:
        """List all GitLab projects in the given groups, do nothing if groups list is empty"""
        if not groups:
            return
        logging.info("loading groups")
        for group in groups:
            for group_obj in self.gitlab.groups.list(search=group, iterator=True):
                logging.info("loading projects from group %s", group_obj.full_path)
                for p in group_obj.projects.list(
                    archived=archived,
                    visibility="public",
                    iterator=True,
                    order_by=order_by,
                    sort="asc",
                ):
                    if reload_project:
                        yield self.gitlab.projects.get(p.id)
                    else:
                        yield p

    def projects(
        self,
        projects: list[str] = [],
        archived: Optional[bool] = None,
        order_by: Optional[str] = None,
    ) -> Iterator[Project]:
        """List Gitlab projects based on the list of project names provided.

        If projects is empty, return all projects."""
        if not projects:
            logging.info("loading all projects")
            # this *is* a list of Project objects, but somehow the
            # type annotation in gitlab is RESTObject, so let's just
            # ignore this type error for now
            yield from self.gitlab.projects.list(
                iterator=True, archived=archived, order_by=order_by, sort="asc"
            )  # type: ignore[misc]
        for project in projects:
            p = self.gitlab.projects.get(project)
            logging.debug("loaded project %s", p.attributes)
            yield p

    def get_group_or_create(
        self, path: str, name: Optional[str] = None, recurse: bool = True
    ):
        try:
            group = self.gitlab.groups.get(path)
        except gitlab.exceptions.GitlabError as e:
            if "Group Not Found" in str(e):
                return self.create_group(path, name, recurse)
            else:
                raise e
        return group

    def create_group(self, path: str, name: Optional[str] = None, recurse: bool = True):
        namespace, path = path.rsplit("/", maxsplit=1)
        try:
            parent_group = self.gitlab.groups.get(namespace)
        except gitlab.exceptions.GitlabError as e:
            if "Group Not Found" in str(e):
                if recurse:
                    parent_group = self.create_group(namespace, recurse=True)
                else:
                    raise ValueError("multi-level subgroups and recurse not used")
            else:
                raise e
        logging.info(
            "creating group %s (%s) with parent %s (%s)",
            path,
            name,
            parent_group.path,
            parent_group.id,
        )
        return self.gitlab.groups.create(
            {"name": name, "path": path, "parent_id": parent_group.id}
        )


@task(
    help={
        "path": "project path, required",
        "namespace": "namespace / group ID, defaults to user",
        "description": "project description, defaults to empty",
        "name": "project name, human-readable, defaults to path",
        "import_url": "when creating repo, import the Git repository from the given URL",
        "archive": "archive the project after creation",
        "gitlab_id": "GitLab instance identifier from configuration file, default specified in configuration file",
        "config_files": "Path to the python-gitlab configuration file, default is typically ~/.python-gitlab.cfg",
    },
)
def create_project(
    con: Connection,
    path: str,
    namespace: Optional[int | str] = None,
    description: Optional[str] = None,
    name: Optional[str] = None,
    import_url: Optional[str] = None,
    archive: bool = False,
    gitlab_id: Optional[str] = None,
    config_files: Optional[List[str]] = None,
    create_missing_groups: bool = False,
) -> Project:
    """Create a GitLab project

    This will optionnally import from another Git repository and
    archive the project as well. If no `namespace` is given, it is
    guessed from the `path`, by taking whatever is left of the last
    slash (`/`) in the path. If namespace is a string, groups are
    searched for a group named by that namespace. If it's an integer,
    that is used directly and is the least ambiguous and most reliable
    way to specify a namespace. By default, the user namespace is used
    if non is specified.

    """
    gitlab_connector = GitLabConnector(
        gitlab_id,
        config_files,
    )
    gl = gitlab_connector.gitlab
    # accept path arguments with a namespace path
    namespace_path = None
    if namespace is None and "/" in path:
        namespace, path = path.rsplit("/", maxsplit=1)
    if namespace is None:
        namespace_id = None
    else:
        try:
            namespace_id = int(namespace)
        except ValueError:
            logging.info("loading group %s", namespace)
            try:
                n = gl.groups.get(namespace)
            except gitlab.exceptions.GitlabError as e:
                if "Group Not Found" in str(e):
                    if create_missing_groups:
                        logging.info("group not found, creating")
                        n = gitlab_connector.create_group(str(namespace), recurse=True)
                    else:
                        raise e
            namespace_id = n.id
            namespace_path = n.full_path

    params = {
        "path": path,
        "namespace_id": namespace_id,
        "description": description,
        "name": name,
        "import_url": import_url,
    }
    logging.info(
        "creating repository %s (%s) in namespace %s%s into %s",
        path,
        name,
        namespace,
        " from %s" % import_url if import_url else "",
        join(gl.url, namespace_path, path) if namespace_path else gl.url,
    )
    try:
        project = gl.projects.create(params)
        assert isinstance(project, Project)
    except gitlab.exceptions.GitlabError as e:
        raise Exit(f"failed to create project: {e}")
    if archive:
        logging.info("archiving project")
        project.archive()
    return project


@task(
    help={
        "username": "username of the new user",
        "email": "email of the new user",
        "gitlab_id": "GitLab instance identifier from configuration file, default specified in configuration file",
        "config_files": "Path to the python-gitlab configuration file, default is typically ~/.python-gitlab.cfg",
    },
)
def create_user(
    con: Connection,
    username: str,
    email: str,
    gitlab_id: Optional[str] = None,
    config_files: Optional[List[str]] = None,
) -> User:
    """Create a GitLab user"""
    gitlab_connector = GitLabConnector(
        gitlab_id,
        config_files,
    )
    gl = gitlab_connector.gitlab
    logging.info("creating user %s with email %s", username, email)
    try:
        user = gl.users.create(
            {
                "email": email,
                "username": username,
                "name": username,
                "force_random_password": True,
            }
        )
        assert isinstance(user, User)
    except gitlab.exceptions.GitlabError as e:
        raise Exit(f"failed to create user: {e}")
    return user


@task(
    help={
        "path": "project path, required",
        "gitlab_id": "GitLab instance identifier from configuration file, default specified in configuration file",
        "config_files": "Path to the python-gitlab configuration file, default is typically ~/.python-gitlab.cfg",
    }
)
def delete_project(
    con: Connection,
    path: str,
    name: Optional[str] = None,
    gitlab_id: Optional[str] = None,
    config_files: Optional[List[str]] = None,
) -> None:
    """delete a project, after confirmation on the terminal"""
    gitlab_connector = GitLabConnector(
        gitlab_id,
        config_files,
    )
    gl = gitlab_connector.gitlab
    logging.info("loading project %s", path)
    try:
        project = gl.projects.get(path)
    except gitlab.exceptions.GitlabError as e:
        raise Exit(f"failed to load project: {e}")
    if no_yes(
        f'really delete project {project.path_with_namespace} ("{project.name}")?'
    ):
        logging.info("deleting project %s", path)
        project.delete()


@task(
    help={
        "projects": "list of projects to inspect, default is all accessible projects",
        "groups": "list of groups to use to find projects",
        "limit": "limit inspection to N projects",
        "order_by": "sort order, see https://python-gitlab.readthedocs.io/en/stable/gl_objects/projects.html#examples",
        "gitlab_id": "GitLab instance identifier from configuration file, default specified in configuration file",
        "config_files": "Path to the python-gitlab configuration file, default is typically ~/.python-gitlab.cfg",
    },
    iterable=["projects", "groups"],
)
def list_projects(
    con: Connection,
    projects: list[str] = [],
    groups: list[str] = [],
    order_by: Optional[str] = None,
    limit: Optional[int] = None,
    gitlab_id: Optional[str] = None,
    config_files: Optional[List[str]] = None,
    archived: Optional[bool] = False,
):
    if groups and projects:
        Exit("specified either a list of projects or groups")
    gitlab_connector = GitLabConnector(
        gitlab_id,
        config_files,
    )
    if groups:
        projects_list = gitlab_connector.group_projects(
            groups,
            archived=archived,
            order_by=order_by,
            reload_project=False,
        )
    else:
        projects_list = gitlab_connector.projects(
            projects,
            order_by=order_by,
            archived=archived,
        )
    count = 0
    for project in projects_list:
        print(project.path_with_namespace)
        if limit and count > limit:
            break
        count += 1


@task
def validate_triage_ops(
    con: Context,
    group: str = "tpo/tpa",
    triage_ops_dir: str = ".",
    gitlab_id: Optional[str] = None,
    config_files: Optional[List[str]] = None,
    fix: bool = False,
    template_dir: str = ".template",
):
    gitlab_connector = GitLabConnector(
        gitlab_id,
        config_files,
    )
    projects_list = gitlab_connector.group_projects(
        [group],
        order_by="path",
        archived=False,
        reload_project=False,
    )
    base_dir = join(expanduser(triage_ops_dir), "policies")
    # gitlab_projects = set()
    for project in projects_list:
        project_path = project.path_with_namespace
        if not project_path.startswith(group):
            continue
        path = join(base_dir, project_path)
        # gitlab_projects.add(project_path)

        logging.debug("checking for project %s in %s", project_path, path)
        if exists(path):
            continue
        if not fix:
            logging.warning("project %s not managed", project_path)
            continue
        logging.info("project %s not managed, adding symlink to %s", project_path, path)
        try:
            os.symlink(template_dir, path)
        except OSError:
            logging.error("failed to create symlink in %s, missing subdirectory?", path)
            continue

    # XXX: we're not checking the triage-ops repo for extra entries,
    # assuming it will fail properly when that happens. we'd need to
    # iterate over base_dir to match only entries that match the
    # "group" and compare that set with gitlab_projects


@task
def validate_mrconfig(
    con: Connection,
    path: str,
    group: str = "tpo/web",
    gitlab_id: Optional[str] = None,
    config_files: Optional[List[str]] = None,
):
    mr_projects_list = set([x.replace(".wiki", "") for x in mrconfig_projects(path)])
    logging.debug("mr projects list: %s", mr_projects_list)
    gitlab_connector = GitLabConnector(
        gitlab_id,
        config_files,
    )
    logging.info("found %d projects in %s", len(mr_projects_list), path)
    gl_projects_list = set(
        [
            x.path_with_namespace.replace(group + "/", "")
            for x in gitlab_connector.group_projects(
                [group],
                archived=False,
                reload_project=False,
            )
            if not x.path_with_namespace.endswith("/repos")
            and x.path_with_namespace.startswith(group)
        ]
    )
    logging.debug("gl projects list: %s", gl_projects_list)
    logging.info("found %d projects in GitLab group %s", len(gl_projects_list), group)
    in_mr_not_in_gl = [x for x in mr_projects_list if x not in gl_projects_list]
    in_gl_not_in_mr = [x for x in gl_projects_list if x not in mr_projects_list]
    if not in_mr_not_in_gl and not in_gl_not_in_mr:
        Exit("projects lists identical")
    if in_mr_not_in_gl:
        logging.warning("in .mrconfig but not GitLab: %s", ", ".join(in_mr_not_in_gl))
    if in_gl_not_in_mr:
        logging.warning("in GitLab but not .mrconfig: %s", ", ".join(in_gl_not_in_mr))
    exit("ERROR: sets differ")


def mrconfig_projects(path: str) -> Iterator[str]:
    config = configparser.ConfigParser()
    config.read(path)

    for section in config.sections():
        if not (
            config.has_option(section, "deleted")
            and config.getboolean(section, "deleted")
        ):
            yield section


@dataclass
class WikiCounter:
    projects = 0
    wikis_empty = 0
    wikis_empty_list: list[str] = field(default_factory=list)
    wikis_nonempty = 0
    wikis_missing = 0
    pages = 0
    pages_map: dict[str, int] = field(default_factory=lambda: defaultdict(int))


@task(
    help={
        "projects": "list of projects to inspect, default is all accessible projects",
        "groups": "list of groups to use to find projects",
        "limit": "limit inspection to N projects",
        "gitlab_id": "GitLab instance identifier from configuration file, default specified in configuration file",
        "config_files": "Path to the python-gitlab configuration file, default is typically ~/.python-gitlab.cfg",
    },
    iterable=["projects", "groups"],
)
def wikis_list(
    con: Connection,
    projects: list[str] = [],
    groups: list[str] = [],
    limit: Optional[int] = None,
    gitlab_id: Optional[str] = None,
    config_files: Optional[List[str]] = None,
):
    if groups and projects:
        Exit("specified either a list of projects or groups")
    gitlab_connector = GitLabConnector(
        gitlab_id,
        config_files,
    )
    if groups:
        projects_list = gitlab_connector.group_projects(groups, archived=False)
    else:
        projects_list = gitlab_connector.projects(projects)
    count = WikiCounter()
    for project in projects_list:
        logging.debug("project: %s", project)
        count.projects += 1
        try:
            pages = project.wikis.list()
        except gitlab.exceptions.GitlabListError as e:
            logging.warning(
                "cannot list wiki pages on project %s: %s",
                project.path_with_namespace,
                e,
            )
            count.wikis_missing += 1
            continue
        if len(pages) == 0:
            logging.warning("empty wiki: %s", project.path_with_namespace)
            count.wikis_empty_list.append(project.path_with_namespace)
            count.wikis_empty += 1
            continue
        count.wikis_nonempty += 1
        logging.info("project %s has %d pages", project.path_with_namespace, len(pages))
        count.pages += len(pages)
        count.pages_map[project.path_with_namespace] = len(pages)
        if limit and count.projects > limit:
            break

    logging.info("empty wikis: %r", count.wikis_empty_list)
    logging.info("non-empty pages map dump: %r", dict(count.pages_map))
    top_10 = sorted(count.pages_map.items(), key=lambda item: item[1])[-10:]
    logging.info("top 10 largest wikis: %r", top_10)

    logging.info("%d projects", count.projects)
    logging.info("%d without wikis", count.wikis_missing)
    logging.info("%d empty wikis", count.wikis_empty)
    logging.info("%d non-empty wikis", count.wikis_nonempty)
    logging.info("%d pages total", count.pages)


@task(
    help={
        "gitlab_id": "GitLab instance identifier from configuration file, default specified in configuration file",
        "config_files": "Path to the python-gitlab configuration file, default is typically ~/.python-gitlab.cfg",
        "comment": "Create a reminder comment on the pipeline's associated merge request",
        "notify": "Send an IRC notification message",
        "minimum": "Minimum build age before a notification is sent, defaults to 90 minutes",
        "maximum": "Maximum age before a notification is sent, defaults to job frequency (2h) plus the minimum (90m)",
    },
)
def undeployed_builds(
    con: Connection,
    gitlab_id: Optional[str] = None,
    config_files: Optional[List[str]] = None,
    comment: bool = False,
    notify: bool = False,
    minimum: int = 90,
    maximum: int = 90 + 120,
) -> None:
    """report deploy-prod jobs waiting for a manual deployment"""
    # The default min/max here are worth exploring in details. We
    # assume we're being called every 2 hours (120m) by some sort of
    # cron job.
    #
    # In the diagram below, builds A to F successively get triggered
    # at 30 minutes interval. The dots show the 90 minutes minimum
    # delay before notification, the pipe (vertical bar) when that
    # threshold has been crossed, and the dashes shows the range
    # between the minimum and maximum, another 2h delay. The J to M
    # markers show when the jobs get run, and below the timings are
    # when the notifications get sent for each job.
    #
    #                               F......90m........|-----------2h----------|
    #                         E.................|-----------------------|
    #                   D.................|-----------------------|
    #             C.................|-----------------------|
    #       B.................|-----------------------|
    # A.................|-----------------------|
    # J                       K                       L                       M
    # 0h00m 0h30m 1h00m 1h30m 2h00m 2h30m 3h00m 3h30m 4h00m 4h30m 5h00m 5h30m 6h00m
    # A                       AB                      BCDE                    F
    #
    # here, we send a single notification for every job except B,
    # which get sent at most twice (on jobs K and L).
    #
    # If we lower maximum threshold below two hours, then B might miss
    # a notfication is job K runs a little too early or L runs a
    # little too late.
    #
    # With a 90m + 2h maximum, jobs would *both* need to be mis-timed
    # for the notification to be missed: K would need to run too early
    # *and* L would need to run too late.
    #
    # The most likely failure mode for this is something else
    # anyway. What is most likely is that the maximum will get out of
    # sync with the job frequency and this will be thrown completely
    # out of whack, missing notifications completely.
    assert minimum < maximum, "minimum should be lower than maximum"

    gitlab_connector = GitLabConnector(
        gitlab_id,
        config_files,
    )
    gl = gitlab_connector.gitlab

    project_list = gl.projects.list(membership=True)

    for project in project_list:
        deploy_job = None
        logging.info("project '%s'", project.path_with_namespace)

        # get latest pipeline on default branch
        try:
            latest_pipeline = project.pipelines.get("latest")
        except gitlab.exceptions.GitlabError as e:
            logging.error("failed to get latest pipeline: %s", e)
            continue

        logging.info("├ found latest pipeline id: %d", latest_pipeline.id)

        # check if a deploy-prod job exists with manual status
        for job in latest_pipeline.jobs.list():
            if job.name == "deploy-prod" and job.status == "manual":
                deploy_job = job
                break

        if not deploy_job:
            logging.info("├ no pending 'deploy-prod' job in this project")
            continue

        logging.info("├ found pending 'deploy-prod' job with id: %s", deploy_job.id)

        # check job age, skip if recent
        updated_at = datetime.datetime.fromisoformat(deploy_job.pipeline["updated_at"])
        now = datetime.datetime.now(timezone.utc)
        delta = now - updated_at

        if delta.total_seconds() / 60 < minimum:
            logging.info("└ job is less than %d minutes old, skipping", minimum)
            continue
        if delta.total_seconds() / 60 > maximum:
            logging.info("└ job is more than %d minutes old, skipping", maximum)
            continue

        if notify:
            notify_msg = f"Found a pending \033[91mdeploy-prod\033[0m job in \033[92m{project.path_with_namespace}\033[0m, to run job visit \033[4m{deploy_job.web_url}\033[0m"  # noqa: E501
            logging.info(
                '├ ⚠ deploy-prod job has been pending > %d minutes, sending notification: "%s"',
                minimum,
                notify_msg,
            )
            kgb_relay_message(notify_msg, callback=lambda _: None)
        else:
            logging.info(
                "├ ⚠ deploy-prod job has been pending > %s minutes, use --notify to send notification",
                minimum,
            )

        if comment:
            commit = project.commits.get(latest_pipeline.sha)

            for mr in commit.merge_requests():
                merge_request = project.mergerequests.get(mr["iid"])

                for note in merge_request.notes.list():
                    if note.author["username"] == "ci-housekeeper":
                        logging.info(
                            "├ bot %s left a comment on merge request %s, skipping",
                            note.author["username"],
                            mr["iid"],
                        )
                        break
                    # don't leave more than one reminder comment on mr
                else:
                    note_msg = f"The CI build for this MR is now finished and the `deploy-prod` job is now pending.\n\nPlease consider deploying by clicking the `Run job` button on the [job page]({deploy_job.web_url})."  # noqa: E501
                    logging.info(
                        "├ leaving a comment on merge request %s: %s",
                        mr["iid"],
                        note_msg,
                    )
                    merge_request.notes.create({"body": note_msg})
