import atexit
import datetime
import hashlib

import logging
import os.path
import re
import signal
import sys

from typing import Optional

try:
    from humanize import naturalsize
except ImportError:
    sys.stderr.write("cannot import humanize, sizes will be ugly\n")

    def naturalsize(
        value: float | str, binary: bool = False, gnu: bool = False, format: str = ""
    ) -> str:
        return str(value) + "B"


try:
    from fabric import Config, Connection
    from fabric.main import Fab, Executor
except ImportError:
    sys.stderr.write(
        "cannot find fabric, install with `apt install python3-fabric`"
    )  # noqa: E501
    raise

from paramiko.client import RejectPolicy
from invoke import Argument


# some gymnastics to reimplement the nice extra features hexlify has
# in python 3.8, but unfortunately missing in earlier versions
from binascii import hexlify as stdlib_hexlify


def local_hexlify(data, sep, bytes_per_sep=1):
    """
    replacement for python 3.8's hexlify, which now nicely takes a separator

    data and sep are bytes, and it returns bytes

    this is typically used to decode a checksum into a human-readable form

    >>> local_hexlify(b'0000', b':', 2)
    b'30:30:30:30'
    """
    # turn bytes into hex
    s = stdlib_hexlify(data)
    # take the bytes and split them with the separator:
    # 1. take a byte and the next N: s[i:i+bytes_per_sep]
    # 2. for each byte, skipping N: range(0, len(s), bytes_per_sep)
    # 3. rejoin by on the separator: sep.join
    return sep.join(s[i: i + bytes_per_sep] for i in range(0, len(s), bytes_per_sep))  # fmt: skip


if sys.version_info >= (3, 8):
    hexlify = stdlib_hexlify
else:
    hexlify = local_hexlify


class VerboseProgram(Fab):
    """Fabric program with a --quiet/-q flag.

    This overrides the builtin fabric.Fab class to set verbose (INFO
    level) logging by default. It also adds a --quiet commandline
    argument to the parser to revert back to the normal Fabric
    default.

    This is called a Program because that is how invoke calls that
    class. I do not know why Fabric diverged on that point and it
    seemed clearer to call this a "program" instead.

    This has been proposed upstream as:

    https://github.com/pyinvoke/invoke/pull/706

    """

    def __init__(self, *args, executor_class=Executor, config_class=Config, **kwargs):
        """Add proper defaults to `__init__`

        The two overriden parameters here are only set in fabric.main,
        not in the fabric.Fab constructor. So override parameters here
        do not properly get set otherwise.

        Cargo-culted from fab's main.py"""
        super().__init__(
            *args, executor_class=executor_class, config_class=config_class, **kwargs
        )

    def core_args(self):
        """Add the extra verbose Argument to the commandline parser"""
        core_args = super().core_args()
        extra_args = [
            Argument(
                names=("quiet", "q"),
                kind=bool,
                default=False,
                help="show WARNINGs only (default to show INFO level output)",
            ),
            Argument(
                names=("verbose", "v"),
                kind=bool,
                default=False,
                help="be more verbose (deprecated, now default)",
            ),
        ]
        return core_args + extra_args

    def parse_core(self, argv):
        """setup logging and a timer

        This reacts to the '--debug' and '--quiet' flags to setup
        proper levels in the `logging` module. It also sets up a
        Timer() to report on how long jobs take in general.
        """

        # override basic format
        logging.basicConfig(format="%(message)s")
        super().parse_core(argv)
        if self.args.debug.value:
            logging.getLogger("").setLevel(logging.DEBUG)
        elif self.args.verbose.value:
            logging.warning("--verbose is now deprecated and the default")
            logging.getLogger("").setLevel(logging.INFO)
        elif self.args.quiet.value:
            logging.getLogger("").setLevel(logging.WARNING)
        else:
            logging.getLogger("").setLevel(logging.INFO)

        # override default logging policies in submodules
        #
        # without this, we get debugging info from paramiko with --verbose
        for mod in "fabric", "paramiko", "invoke":
            logging.getLogger(mod).setLevel("WARNING")

        # set a timer
        self._tpa_timer = Timer()
        logging.info("starting tasks at %s", self._tpa_timer.stamp)
        atexit.register(self._tpa_log_completion)

    def _tpa_log_completion(self):
        """atexit handler that runs at the end of the program

        There should be a better way to do this in Program, but there
        are no post-execution hooks anywhere that I could find."""
        logging.info("completed tasks, %s", self._tpa_timer)


Connection.default_host_key_policy = RejectPolicy


# hack to fix Fabric key policy:
# https://github.com/fabric/fabric/issues/2071
def safe_open(self):
    SaferConnection.setup_ssh_client(self)
    Connection.open_orig(self)


class SaferConnection(Connection):
    # this function is a copy-paste from
    # https://github.com/fabric/fabric/pull/2072
    def setup_ssh_client(self):
        if self.default_host_key_policy is not None:
            logging.debug("host key policy: %s", self.default_host_key_policy)
            self.client.set_missing_host_key_policy(self.default_host_key_policy())
        known_hosts = self.ssh_config.get(
            "UserKnownHostsFile".lower(), "~/.ssh/known_hosts  ~/.ssh/known_hosts2"
        )
        logging.debug("loading host keys from %s", known_hosts)
        # multiple keys, seperated by whitespace, can be provided
        for filename in [os.path.expanduser(f) for f in known_hosts.split()]:
            if os.path.exists(filename):
                self.client.load_host_keys(filename)


Connection.open_orig = Connection.open
Connection.open = safe_open


class Timer(object):
    """this class is to track time and resources passed

    originally from bup-cron, but improved to include memory usage"""

    def __init__(self):
        """initialize the timstamp"""
        self.stamp = datetime.datetime.now(datetime.timezone.utc)

    def times(self):
        """return a string designing resource usage"""
        return "user %s system %s chlduser %s chldsystem %s" % os.times()[:4]

    def rss(self):
        import psutil

        process = psutil.Process(os.getpid())
        return process.memory_info().rss

    def memory(self):
        return "RSS %s" % naturalsize(self.rss())

    def diff(self):
        """a datediff between the creation of the object and now"""
        return datetime.datetime.now(datetime.timezone.utc) - self.stamp

    def __str__(self):
        """return a string representing the time passed and resources used"""
        return "elasped: %s (%s %s)" % (str(self.diff()), self.times(), self.memory())


def hash_digest_hex(data, hash=hashlib.md5, sep=":"):
    return hexlify(hash(data).digest(), sep, 2)


def no_yes(prompt: str) -> bool:
    """ask a yes/no question, defaulting to no. Return False on no, True on yes"""
    while True:
        res = input(prompt + "\a [N/y] ").lower()
        if res and res not in "yn":
            print("invalid response, must be one of y or n")
            continue
        if not res or res != "y":
            return False
        break
    return True


def yes_no(prompt: str) -> bool:
    """ask a yes/no question, defaulting to yes. Return False on no, True on yes"""
    while True:
        res = input(prompt + "\a [Y/n] ").lower()
        if res and res not in "yn":
            print("invalid response, must be one of y or n")
            continue
        if not res or res != "n":
            return True
        break
    return False


def pick(
    options: Optional[list[str]], prompt: str = "select entry above or enter to skip: "
) -> Optional[str]:
    if not options:
        return None
    if len(options) == 1:
        return options[0]
    for i, p in enumerate(options):
        print(i, p)
    while True:
        sel = input(prompt + "\a").strip()
        if sel == "":
            return None
        try:
            return options[int(sel)]
        except (ValueError, IndexError) as e:
            if sel in options:
                return sel
            print("invalid choice:", str(e))
            continue


def re_include_exclude(
    content: Optional[str], include: Optional[str] = None, exclude: Optional[str] = None
) -> bool:
    if not content:
        return True
    if exclude and re.search(exclude, content):
        return False
    if include and not re.search(include, content):
        return False
    return True


def input_bell(prompt: str = "press enter to continue: ") -> str:
    return input("\a" + prompt)


def confirm_with_timeout(prompt: str, timeout_secs: int) -> bool:
    def handler(signum, frame):
        raise TimeoutError()

    signal.signal(signal.SIGALRM, handler)
    signal.alarm(timeout_secs)
    try:
        input_bell(prompt)
        signal.alarm(0)
    except TimeoutError:
        # need to send a newline to wrap our prompt because the user didn't input
        print()
        return False

    return True
