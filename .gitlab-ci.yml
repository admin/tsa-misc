# We use a job template to avoid mindless repetition.  All of our jobs
# run 'tox', and we vary which tox environment they run or which
# Python version they use by overriding the 'TOXENV' environment
# variable or the 'image' job parameter.
#
# The name of the template starts with a dot to prevent GitLab from
# running it as an actual job.
.tox:
  # python 3.13
  image: containers.torproject.org/tpo/tpa/base-images/python:trixie
  before_script:
    - apt update && apt install -y tox libldap-dev libsasl2-dev
  script:
    - tox
  variables:
    TOXENV: py
    # The environment 'py' uses the version of Python used to invoke tox,
    # which allows us to test various Python versions by changing the
    # Docker image version in use.

# the main test suite, for each python version
test:
  extends: .tox
  # match this list of python versions with the classifiers variable
  # in setup.py. see also the list in .gitlab-ci.yml, which is
  # specific to the docker container running the tests
  parallel:
    matrix:
      - IMAGE:
          # python 3.11
          - containers.torproject.org/tpo/tpa/base-images/python:bookworm
          # python 3.13
          - containers.torproject.org/tpo/tpa/base-images/python:trixie
  image: $IMAGE

flake8:
  extends: .tox
  variables:
    TOXENV: flake8

black:
  extends: .tox
  variables:
    TOXENV: black

mypy:
  extends: .tox
  variables:
    TOXENV: mypy
  script:
    - tox -- --install-types --non-interactive .

mypy-strict:
  extends: .tox
  variables:
    TOXENV: mypy
  script:
    - tox -- --strict --install-types --non-interactive .
  allow_failure: true

# audit TPA projects for missing web hooks
#
# expects the GITLAB_PRIVATE_TOKEN environment
check-tpa-projects:
  image: containers.torproject.org/tpo/tpa/base-images/debian:stable
  script:
    - apt update && apt install -yy python3-gitlab python3-fabric
    - ./gitlab-hooks.py -g tpo/tpa audit
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"

# audit TPA projects for misconfigured .mrconfig
#
# note that this will fail until we either manually restrict the .mrconfig
# files to only repos in the corresponding gitlab groups or change the
# validation routine to only check against repositories hosted in our GitLab.
#
# expects the GITLAB_PRIVATE_TOKEN environment
check-tpa-mrconfig:
  image: containers.torproject.org/tpo/tpa/base-images/debian:stable
  script:
    - apt update && apt install -yy python3-gitlab python3-fabric git
    - git clone --depth=1 https://gitlab.torproject.org/tpo/web/repos web-repos
    - fab gitlab.validate-mrconfig --group tpo/web --path web-repos/.mrconfig
    - git clone --depth=1 https://gitlab.torproject.org/tpo/tpa/repos tpa-repos
    - fab gitlab.validate-mrconfig --group tpo/tpa --path tpa-repos/.mrconfig
  rules:
    - if: $CI_PIPELINE_SOURCE == "schedule"
  allow_failure: true
